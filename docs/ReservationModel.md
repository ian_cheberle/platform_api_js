# PlatformApi.ReservationModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reservationId** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**status** | **String** |  | [optional] 


