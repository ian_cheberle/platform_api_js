# PlatformApi.CounterlessRentalsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRentalHttpControllersCounterlessRentalControllerCreateRental**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerCreateRental) | **POST** /{tenant}/counterless/rentals | Creates a single counterless rental
[**modulesRentalHttpControllersCounterlessRentalControllerDeleteRental**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerDeleteRental) | **DELETE** /{tenant}/counterless/rentals/{ra_number} | Deletes an specific Counterless Rental
[**modulesRentalHttpControllersCounterlessRentalControllerGetPhotos**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerGetPhotos) | **GET** /{tenant}/counterless/rental/{ra_number}/photos | Get all Photos by RA Number
[**modulesRentalHttpControllersCounterlessRentalControllerGetRental**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerGetRental) | **GET** /{tenant}/counterless/rentals/{ra_number} | Finds a single counterless rental
[**modulesRentalHttpControllersCounterlessRentalControllerGetRentals**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerGetRentals) | **GET** /{tenant}/counterless/rentals | Get all counterless rentals
[**modulesRentalHttpControllersCounterlessRentalControllerUpdateRental**](CounterlessRentalsApi.md#modulesRentalHttpControllersCounterlessRentalControllerUpdateRental) | **PATCH** /{tenant}/counterless/rentals/{ra_number} | Update an specific Counterless Rental
[**modulesRenterUserHttpControllersRenterRenterControllerGetPhotos**](CounterlessRentalsApi.md#modulesRenterUserHttpControllersRenterRenterControllerGetPhotos) | **GET** /{tenant}/counterless/renters/{renterId}/photos | Get all Photos by Renter Id
[**modulesRenterUserHttpControllersRenterRenterControllerGetRentals**](CounterlessRentalsApi.md#modulesRenterUserHttpControllersRenterRenterControllerGetRentals) | **GET** /{tenant}/counterless/renters/{renterId}/rentals | Get all counterless rentals related to a renter



## modulesRentalHttpControllersCounterlessRentalControllerCreateRental

> InlineResponse2013 modulesRentalHttpControllersCounterlessRentalControllerCreateRental(tenant, createCounterlessRentalAcceptableBodyRequest)

Creates a single counterless rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createCounterlessRentalAcceptableBodyRequest = new PlatformApi.CreateCounterlessRentalAcceptableBodyRequest(); // CreateCounterlessRentalAcceptableBodyRequest | Rental object that needs to be added to the platform
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerCreateRental(tenant, createCounterlessRentalAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createCounterlessRentalAcceptableBodyRequest** | [**CreateCounterlessRentalAcceptableBodyRequest**](CreateCounterlessRentalAcceptableBodyRequest.md)| Rental object that needs to be added to the platform | 

### Return type

[**InlineResponse2013**](InlineResponse2013.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalControllerDeleteRental

> AnyType modulesRentalHttpControllersCounterlessRentalControllerDeleteRental(tenant, raNumber)

Deletes an specific Counterless Rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerDeleteRental(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalControllerGetPhotos

> AllPhotosPaginated modulesRentalHttpControllersCounterlessRentalControllerGetPhotos(tenant, raNumber, opts)

Get all Photos by RA Number

Returns photos data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | RA Number
let opts = {
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerGetPhotos(tenant, raNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| RA Number | 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllPhotosPaginated**](AllPhotosPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalControllerGetRental

> CounterlessRentalModel modulesRentalHttpControllersCounterlessRentalControllerGetRental(tenant, raNumber)

Finds a single counterless rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement Number
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerGetRental(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement Number | 

### Return type

[**CounterlessRentalModel**](CounterlessRentalModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalControllerGetRentals

> AllCounterlessRentalsPaginated modulesRentalHttpControllersCounterlessRentalControllerGetRentals(tenant, opts)

Get all counterless rentals

Returns counterless rental data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rentalStatus': ["null"], // [String] | 
  'counterlessStatus': ["null"], // [String] | 
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'stationStart': "stationStart_example", // String | 
  'stationEnd': "stationEnd_example", // String | 
  'checkedIn': true, // Boolean | 
  'vehicleAssigned': true, // Boolean | 
  'atVehicle': true, // Boolean | 
  'keySwapActioned': true, // Boolean | 
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerGetRentals(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalStatus** | [**[String]**](String.md)|  | [optional] 
 **counterlessStatus** | [**[String]**](String.md)|  | [optional] 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **stationStart** | **String**|  | [optional] 
 **stationEnd** | **String**|  | [optional] 
 **checkedIn** | **Boolean**|  | [optional] 
 **vehicleAssigned** | **Boolean**|  | [optional] 
 **atVehicle** | **Boolean**|  | [optional] 
 **keySwapActioned** | **Boolean**|  | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllCounterlessRentalsPaginated**](AllCounterlessRentalsPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalControllerUpdateRental

> AnyType modulesRentalHttpControllersCounterlessRentalControllerUpdateRental(tenant, raNumber, updateCounterlessRentalAcceptableBodyRequest)

Update an specific Counterless Rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
let updateCounterlessRentalAcceptableBodyRequest = new PlatformApi.UpdateCounterlessRentalAcceptableBodyRequest(); // UpdateCounterlessRentalAcceptableBodyRequest | Counterless Rental values to be updated
apiInstance.modulesRentalHttpControllersCounterlessRentalControllerUpdateRental(tenant, raNumber, updateCounterlessRentalAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 
 **updateCounterlessRentalAcceptableBodyRequest** | [**UpdateCounterlessRentalAcceptableBodyRequest**](UpdateCounterlessRentalAcceptableBodyRequest.md)| Counterless Rental values to be updated | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRenterUserHttpControllersRenterRenterControllerGetPhotos

> AllPhotosPaginated modulesRenterUserHttpControllersRenterRenterControllerGetPhotos(tenant, renterId, opts)

Get all Photos by Renter Id

Returns photos data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter Id (Driver ID)
let opts = {
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerGetPhotos(tenant, renterId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter Id (Driver ID) | 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllPhotosPaginated**](AllPhotosPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersRenterRenterControllerGetRentals

> AllCounterlessRentalsPaginated modulesRenterUserHttpControllersRenterRenterControllerGetRentals(tenant, renterId, opts)

Get all counterless rentals related to a renter

Returns counterless rental data by renter

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter of the request (Driver Id)
let opts = {
  'rentalStatus': ["null"], // [String] | 
  'counterlessStatus': ["null"], // [String] | 
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'stationStart': "stationStart_example", // String | 
  'stationEnd': "stationEnd_example", // String | 
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerGetRentals(tenant, renterId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter of the request (Driver Id) | 
 **rentalStatus** | [**[String]**](String.md)|  | [optional] 
 **counterlessStatus** | [**[String]**](String.md)|  | [optional] 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **stationStart** | **String**|  | [optional] 
 **stationEnd** | **String**|  | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllCounterlessRentalsPaginated**](AllCounterlessRentalsPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

