# PlatformApi.StationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesStationHttpControllersStationControllerCreateStation**](StationsApi.md#modulesStationHttpControllersStationControllerCreateStation) | **POST** /{tenant}/stations | Creates a single station
[**modulesStationHttpControllersStationControllerDeleteStation**](StationsApi.md#modulesStationHttpControllersStationControllerDeleteStation) | **DELETE** /{tenant}/stations/{station_id} | Deletes a single Station
[**modulesStationHttpControllersStationControllerGetStation**](StationsApi.md#modulesStationHttpControllersStationControllerGetStation) | **GET** /{tenant}/stations/{station_id} | Gets an Station based on the station_id
[**modulesStationHttpControllersStationControllerGetStations**](StationsApi.md#modulesStationHttpControllersStationControllerGetStations) | **GET** /{tenant}/stations | Get all stations
[**modulesStationHttpControllersStationControllerUpdateStation**](StationsApi.md#modulesStationHttpControllersStationControllerUpdateStation) | **PATCH** /{tenant}/stations/{station_id} | Update an specific Station



## modulesStationHttpControllersStationControllerCreateStation

> InlineResponse2016 modulesStationHttpControllersStationControllerCreateStation(tenant, createStationAcceptableBodyRequest)

Creates a single station

Returns itself data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.StationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createStationAcceptableBodyRequest = new PlatformApi.CreateStationAcceptableBodyRequest(); // CreateStationAcceptableBodyRequest | Station object that needs to be added to the platform
apiInstance.modulesStationHttpControllersStationControllerCreateStation(tenant, createStationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createStationAcceptableBodyRequest** | [**CreateStationAcceptableBodyRequest**](CreateStationAcceptableBodyRequest.md)| Station object that needs to be added to the platform | 

### Return type

[**InlineResponse2016**](InlineResponse2016.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesStationHttpControllersStationControllerDeleteStation

> modulesStationHttpControllersStationControllerDeleteStation(tenant, stationId)

Deletes a single Station

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.StationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let stationId = "stationId_example"; // String | Station Id
apiInstance.modulesStationHttpControllersStationControllerDeleteStation(tenant, stationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **stationId** | **String**| Station Id | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesStationHttpControllersStationControllerGetStation

> StationModel modulesStationHttpControllersStationControllerGetStation(tenant, stationId)

Gets an Station based on the station_id

Returns a station

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.StationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let stationId = "stationId_example"; // String | 
apiInstance.modulesStationHttpControllersStationControllerGetStation(tenant, stationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **stationId** | **String**|  | 

### Return type

[**StationModel**](StationModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesStationHttpControllersStationControllerGetStations

> StationModel modulesStationHttpControllersStationControllerGetStations(tenant)

Get all stations

Returns array with stations data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.StationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesStationHttpControllersStationControllerGetStations(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**StationModel**](StationModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesStationHttpControllersStationControllerUpdateStation

> modulesStationHttpControllersStationControllerUpdateStation(tenant, stationId, updateStationAcceptableBodyRequest)

Update an specific Station

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.StationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let stationId = "stationId_example"; // String | Station Id
let updateStationAcceptableBodyRequest = new PlatformApi.UpdateStationAcceptableBodyRequest(); // UpdateStationAcceptableBodyRequest | Station values to be updated
apiInstance.modulesStationHttpControllersStationControllerUpdateStation(tenant, stationId, updateStationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **stationId** | **String**| Station Id | 
 **updateStationAcceptableBodyRequest** | [**UpdateStationAcceptableBodyRequest**](UpdateStationAcceptableBodyRequest.md)| Station values to be updated | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

