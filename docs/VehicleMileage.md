# PlatformApi.VehicleMileage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registration** | **String** |  | [optional] 
**imei** | **Number** |  | [optional] 
**epmStart** | **Number** |  | [optional] 
**currentKms** | **Number** |  | [optional] 
**estKms** | **Number** |  | [optional] 


