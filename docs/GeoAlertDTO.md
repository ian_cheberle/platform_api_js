# PlatformApi.GeoAlertDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**imei** | **Number** |  | [optional] 
**timestamp** | **Number** |  | [optional] 
**geofenceId** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**vid** | **String** |  | [optional] 
**geofence** | **String** |  | [optional] 


