# PlatformApi.InlineResponse2007

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Number** | The component we use is not capable working with paginated api results, so we need to load all results in one request. For this reason pagination is not used by the frontend application yet.  We decided to keep basic pagination interface (endpoint params and response structure) so replacing frontend component with a better suited component work is decoupled from backend work. Current response format allows us to extend it with more data which may be required by frontend with backward-compatible increments | [optional] 
**data** | [**[VehicleWithSummaryModel]**](VehicleWithSummaryModel.md) | The component we use is not capable working with paginated api results, so we need to load all results in one request. For this reason pagination is not used by the frontend application yet.  We decided to keep basic pagination interface (endpoint params and response structure) so replacing frontend component with a better suited component work is decoupled from backend work. Current response format allows us to extend it with more data which may be required by frontend with backward-compatible increments | [optional] 


