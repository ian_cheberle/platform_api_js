# PlatformApi.UpdateBranchConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **Number** |  | [optional] 
**directions** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**emailListLicenceApproval** | **[String]** |  | [optional] 
**emailListCheckIn** | **[String]** |  | [optional] 
**emailListKeySwap** | **[String]** |  | [optional] 


