# PlatformApi.ImpactDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**imei** | **Number** |  | [optional] 
**timestamp** | **Number** |  | [optional] 
**severity** | **String** |  | [optional] 
**location** | [**AnyType**](.md) |  | [optional] 
**magnitude** | **Number** |  | [optional] 
**vid** | [**AnyType**](.md) |  | [optional] 
**probability** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**accelerometerPoints** | [**AnyType**](.md) |  | [optional] 


