# PlatformApi.PairDeviceVehicleBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**phoneLat** | **Number** |  | [optional] 
**phoneLon** | **Number** |  | [optional] 
**registration** | **String** |  | [optional] 
**tag** | **String** |  | [optional] 



## Enum: TagEnum


* `replacement` (value: `"replacement"`)

* `off-fleeting` (value: `"off-fleeting"`)

* `on-fleeting` (value: `"on-fleeting"`)




