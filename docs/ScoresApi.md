# PlatformApi.ScoresApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesScoreHttpControllersScoreControllerGetScores**](ScoresApi.md#modulesScoreHttpControllersScoreControllerGetScores) | **GET** /{tenant}/scores | Get all scores
[**modulesScoreHttpControllersScoreControllerGetScoresByType**](ScoresApi.md#modulesScoreHttpControllersScoreControllerGetScoresByType) | **GET** /{tenant}/scores/{score_type} | Get all scores based on the type



## modulesScoreHttpControllersScoreControllerGetScores

> [ScoreDTO] modulesScoreHttpControllersScoreControllerGetScores(tenant)

Get all scores

Returns all scores data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ScoresApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesScoreHttpControllersScoreControllerGetScores(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**[ScoreDTO]**](ScoreDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesScoreHttpControllersScoreControllerGetScoresByType

> [ScoreDTO] modulesScoreHttpControllersScoreControllerGetScoresByType(tenant, scoreType)

Get all scores based on the type

Returns all scores data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ScoresApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let scoreType = ["null"]; // [String] | 
apiInstance.modulesScoreHttpControllersScoreControllerGetScoresByType(tenant, scoreType, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **scoreType** | [**[String]**](String.md)|  | 

### Return type

[**[ScoreDTO]**](ScoreDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

