# PlatformApi.UpdateLicenceAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licenceApproved** | **Boolean** |  | [optional] 
**licenceApprovedUser** | **String** |  | [optional] 
**licenceRejected** | **Boolean** |  | [optional] 
**licenceRejectedUser** | **String** |  | [optional] 
**licenceRejectedReason** | **String** |  | [optional] 


