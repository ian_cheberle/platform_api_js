# PlatformApi.BranchConfigApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesBranchConfigHttpControllersBranchConfigControllerCreateBranchConfigs**](BranchConfigApi.md#modulesBranchConfigHttpControllersBranchConfigControllerCreateBranchConfigs) | **POST** /{tenant}/counterless/branchconfigs | Creates a single Branch Configuration
[**modulesBranchConfigHttpControllersBranchConfigControllerDeleteBranchConfig**](BranchConfigApi.md#modulesBranchConfigHttpControllersBranchConfigControllerDeleteBranchConfig) | **DELETE** /{tenant}/counterless/branchconfigs/{branch_config_id} | Deletes a single Branch Configuration
[**modulesBranchConfigHttpControllersBranchConfigControllerGetAllBranchConfigs**](BranchConfigApi.md#modulesBranchConfigHttpControllersBranchConfigControllerGetAllBranchConfigs) | **GET** /{tenant}/counterless/branchconfigs | Get all branch configs
[**modulesBranchConfigHttpControllersBranchConfigControllerGetBranchConfig**](BranchConfigApi.md#modulesBranchConfigHttpControllersBranchConfigControllerGetBranchConfig) | **GET** /{tenant}/counterless/branchconfigs/{branch_config_id} | Finds a single Branch Configuration
[**modulesBranchConfigHttpControllersBranchConfigControllerUpdateBranchConfig**](BranchConfigApi.md#modulesBranchConfigHttpControllersBranchConfigControllerUpdateBranchConfig) | **PATCH** /{tenant}/counterless/branchconfigs/{branch_config_id} | Update an specific branch configuration



## modulesBranchConfigHttpControllersBranchConfigControllerCreateBranchConfigs

> InlineResponse201 modulesBranchConfigHttpControllersBranchConfigControllerCreateBranchConfigs(tenant, createBranchConfigRequest)

Creates a single Branch Configuration

counterless insertion Branch Configuration data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.BranchConfigApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createBranchConfigRequest = new PlatformApi.CreateBranchConfigRequest(); // CreateBranchConfigRequest | Branch Configuration object that needs to be added to the platform
apiInstance.modulesBranchConfigHttpControllersBranchConfigControllerCreateBranchConfigs(tenant, createBranchConfigRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createBranchConfigRequest** | [**CreateBranchConfigRequest**](CreateBranchConfigRequest.md)| Branch Configuration object that needs to be added to the platform | 

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesBranchConfigHttpControllersBranchConfigControllerDeleteBranchConfig

> modulesBranchConfigHttpControllersBranchConfigControllerDeleteBranchConfig(tenant, branchConfigId)

Deletes a single Branch Configuration

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.BranchConfigApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let branchConfigId = "branchConfigId_example"; // String | Branch Configuration Id
apiInstance.modulesBranchConfigHttpControllersBranchConfigControllerDeleteBranchConfig(tenant, branchConfigId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **branchConfigId** | **String**| Branch Configuration Id | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesBranchConfigHttpControllersBranchConfigControllerGetAllBranchConfigs

> Pagination modulesBranchConfigHttpControllersBranchConfigControllerGetAllBranchConfigs(tenant, opts)

Get all branch configs

Returns all branch data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.BranchConfigApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesBranchConfigHttpControllersBranchConfigControllerGetAllBranchConfigs(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**Pagination**](Pagination.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesBranchConfigHttpControllersBranchConfigControllerGetBranchConfig

> BranchConfigModel modulesBranchConfigHttpControllersBranchConfigControllerGetBranchConfig(tenant, branchConfigId)

Finds a single Branch Configuration

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.BranchConfigApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let branchConfigId = 56; // Number | BranchConfig Id
apiInstance.modulesBranchConfigHttpControllersBranchConfigControllerGetBranchConfig(tenant, branchConfigId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **branchConfigId** | **Number**| BranchConfig Id | 

### Return type

[**BranchConfigModel**](BranchConfigModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesBranchConfigHttpControllersBranchConfigControllerUpdateBranchConfig

> AnyType modulesBranchConfigHttpControllersBranchConfigControllerUpdateBranchConfig(tenant, branchConfigId, createBranchConfigRequest)

Update an specific branch configuration

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.BranchConfigApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let branchConfigId = 56; // Number | Branch Config Id
let createBranchConfigRequest = new PlatformApi.CreateBranchConfigRequest(); // CreateBranchConfigRequest | Branch Config values to be updated
apiInstance.modulesBranchConfigHttpControllersBranchConfigControllerUpdateBranchConfig(tenant, branchConfigId, createBranchConfigRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **branchConfigId** | **Number**| Branch Config Id | 
 **createBranchConfigRequest** | [**CreateBranchConfigRequest**](CreateBranchConfigRequest.md)| Branch Config values to be updated | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

