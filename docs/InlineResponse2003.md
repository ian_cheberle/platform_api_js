# PlatformApi.InlineResponse2003

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rental** | **Number** |  | [optional] 
**driver** | **Number** |  | [optional] 
**vehicle** | **Number** |  | [optional] 
**rentalAssociation** | **Number** |  | [optional] 


