# PlatformApi.UpdatePhotoAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**licenceId** | **Number** |  | [optional] 
**rentalId** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 
**image** | **String** |  | [optional] 



## Enum: TypeEnum


* `licence front` (value: `"licence front"`)

* `licence back` (value: `"licence back"`)

* `selfie` (value: `"selfie"`)

* `car front` (value: `"car front"`)

* `car front left` (value: `"car front left"`)

* `car left` (value: `"car left"`)

* `car back left` (value: `"car back left"`)

* `car back` (value: `"car back"`)

* `car back right` (value: `"car back right"`)

* `car right` (value: `"car right"`)

* `car front right` (value: `"car front right"`)




