# PlatformApi.CounterlessRentalAssociationModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**start** | **String** |  | [optional] 
**end** | **String** |  | [optional] 
**vid** | **String** |  | [optional] 
**currentAssociation** | **Boolean** |  | [optional] 


