# PlatformApi.ReservationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesReservationHttpControllersReservationControllerCreateReservation**](ReservationsApi.md#modulesReservationHttpControllersReservationControllerCreateReservation) | **POST** /{tenant}/reservations | Creates a single reservation
[**modulesReservationHttpControllersReservationControllerDeleteReservation**](ReservationsApi.md#modulesReservationHttpControllersReservationControllerDeleteReservation) | **DELETE** /{tenant}/reservations/{reservation_id} | Deletes a single Reservation
[**modulesReservationHttpControllersReservationControllerGetReservation**](ReservationsApi.md#modulesReservationHttpControllersReservationControllerGetReservation) | **GET** /{tenant}/reservations/{reservation_id} | Gets an Reservation based on the reservation_id
[**modulesReservationHttpControllersReservationControllerGetReservations**](ReservationsApi.md#modulesReservationHttpControllersReservationControllerGetReservations) | **GET** /{tenant}/reservations | Get all reservations
[**modulesReservationHttpControllersReservationControllerUpdateReservation**](ReservationsApi.md#modulesReservationHttpControllersReservationControllerUpdateReservation) | **PATCH** /{tenant}/reservations/{reservation_id} | Update an specific Reservation



## modulesReservationHttpControllersReservationControllerCreateReservation

> InlineResponse2015 modulesReservationHttpControllersReservationControllerCreateReservation(tenant, createReservationAcceptableBodyRequest)

Creates a single reservation

Returns itself data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ReservationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createReservationAcceptableBodyRequest = new PlatformApi.CreateReservationAcceptableBodyRequest(); // CreateReservationAcceptableBodyRequest | Reservation object that needs to be added to the platform
apiInstance.modulesReservationHttpControllersReservationControllerCreateReservation(tenant, createReservationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createReservationAcceptableBodyRequest** | [**CreateReservationAcceptableBodyRequest**](CreateReservationAcceptableBodyRequest.md)| Reservation object that needs to be added to the platform | 

### Return type

[**InlineResponse2015**](InlineResponse2015.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesReservationHttpControllersReservationControllerDeleteReservation

> modulesReservationHttpControllersReservationControllerDeleteReservation(tenant, reservationId)

Deletes a single Reservation

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ReservationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let reservationId = "reservationId_example"; // String | Reservation Id
apiInstance.modulesReservationHttpControllersReservationControllerDeleteReservation(tenant, reservationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **reservationId** | **String**| Reservation Id | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesReservationHttpControllersReservationControllerGetReservation

> ReservationModel modulesReservationHttpControllersReservationControllerGetReservation(tenant, reservationId)

Gets an Reservation based on the reservation_id

Returns a reservation

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ReservationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let reservationId = "reservationId_example"; // String | 
apiInstance.modulesReservationHttpControllersReservationControllerGetReservation(tenant, reservationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **reservationId** | **String**|  | 

### Return type

[**ReservationModel**](ReservationModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesReservationHttpControllersReservationControllerGetReservations

> AllReservationsPaginated modulesReservationHttpControllersReservationControllerGetReservations(tenant, opts)

Get all reservations

Returns reservations data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ReservationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rangeType': ["null"], // [String] | 
  'status': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'driverId': "driverId_example", // String | 
  'email': "email_example", // String | 
  'firstName': "firstName_example", // String | 
  'surname': "surname_example", // String | 
  'phoneNumber': "phoneNumber_example", // String | 
  'stations': ["null"] // [String] | 
};
apiInstance.modulesReservationHttpControllersReservationControllerGetReservations(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **status** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **driverId** | **String**|  | [optional] 
 **email** | **String**|  | [optional] 
 **firstName** | **String**|  | [optional] 
 **surname** | **String**|  | [optional] 
 **phoneNumber** | **String**|  | [optional] 
 **stations** | [**[String]**](String.md)|  | [optional] 

### Return type

[**AllReservationsPaginated**](AllReservationsPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesReservationHttpControllersReservationControllerUpdateReservation

> modulesReservationHttpControllersReservationControllerUpdateReservation(tenant, reservationId, updateReservationAcceptableBodyRequest)

Update an specific Reservation

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.ReservationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let reservationId = "reservationId_example"; // String | Reservation Id
let updateReservationAcceptableBodyRequest = new PlatformApi.UpdateReservationAcceptableBodyRequest(); // UpdateReservationAcceptableBodyRequest | Reservation values to be updated
apiInstance.modulesReservationHttpControllersReservationControllerUpdateReservation(tenant, reservationId, updateReservationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **reservationId** | **String**| Reservation Id | 
 **updateReservationAcceptableBodyRequest** | [**UpdateReservationAcceptableBodyRequest**](UpdateReservationAcceptableBodyRequest.md)| Reservation values to be updated | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

