# PlatformApi.VehiclesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesVehicleHttpControllersVehicleControllerCreateVehicle**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerCreateVehicle) | **POST** /{tenant}/vehicles | Creates a single vehicle
[**modulesVehicleHttpControllersVehicleControllerDeleteVehicle**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerDeleteVehicle) | **DELETE** /{tenant}/vehicles/{vid} | Deletes a single vehicle
[**modulesVehicleHttpControllersVehicleControllerDoorClose**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerDoorClose) | **PUT** /{tenant}/vehicles/{vid}/doorclose | Closes the doors of a vehicle
[**modulesVehicleHttpControllersVehicleControllerDoorOpen**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerDoorOpen) | **PUT** /{tenant}/vehicles/{vid}/dooropen | Opens the doors of a vehicle
[**modulesVehicleHttpControllersVehicleControllerFindVehicle**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerFindVehicle) | **GET** /{tenant}/vehicles/{vid} | Get a single vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleDeviceAlerts**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleDeviceAlerts) | **GET** /{tenant}/vehicles/{vid}/devicealerts | Get all device alerts based on the imei&#39;s
[**modulesVehicleHttpControllersVehicleControllerGetVehicleDevices**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleDevices) | **GET** /{tenant}/vehicles/{vid}/devices | Get devices for a specific vehicle with time range
[**modulesVehicleHttpControllersVehicleControllerGetVehicleGeoAlerts**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleGeoAlerts) | **GET** /{tenant}/vehicles/{vid}/geoalerts | Get all geo alerts based on the imei&#39;s
[**modulesVehicleHttpControllersVehicleControllerGetVehicleIdleEvents**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleIdleEvents) | **GET** /{tenant}/vehicles/{vid}/idleevents | Get idle events for a specific vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleImpacts**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleImpacts) | **GET** /{tenant}/vehicles/{vid}/impacts | Get all impacts based on the imei&#39;s
[**modulesVehicleHttpControllersVehicleControllerGetVehicleJourneys**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleJourneys) | **GET** /{tenant}/vehicles/{vid}/journeys | Get all journeys of a vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleMileage**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleMileage) | **GET** /{tenant}/vehicles/{registration}/mileage | Get mileage for a specific vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleNrms**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleNrms) | **GET** /{tenant}/vehicles/{vid}/nrms | Get nrms for a specific vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehiclePosition**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehiclePosition) | **GET** /{tenant}/vehicles/{vid}/position | Get latest position for a specific vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleRentals**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleRentals) | **GET** /{tenant}/vehicles/{vid}/rentals | Get rentals based on the RA number related with vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicleRepairOrders**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleRepairOrders) | **GET** /{tenant}/vehicles/{vid}/repairorders | Get all repair orders based on the imei&#39;s
[**modulesVehicleHttpControllersVehicleControllerGetVehicleSpeedEvents**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicleSpeedEvents) | **GET** /{tenant}/vehicles/{vid}/speedevents | Get speed events for a specific vehicle
[**modulesVehicleHttpControllersVehicleControllerGetVehicles**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerGetVehicles) | **GET** /{tenant}/vehicles | Get all vehicles except by deleted and sold vehicles
[**modulesVehicleHttpControllersVehicleControllerUpdateVehicle**](VehiclesApi.md#modulesVehicleHttpControllersVehicleControllerUpdateVehicle) | **PATCH** /{tenant}/vehicles/{vid} | Update a specific vehicle



## modulesVehicleHttpControllersVehicleControllerCreateVehicle

> InlineResponse2017 modulesVehicleHttpControllersVehicleControllerCreateVehicle(tenant, createVehicleAcceptableBodyRequest)

Creates a single vehicle

Returns itself data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createVehicleAcceptableBodyRequest = new PlatformApi.CreateVehicleAcceptableBodyRequest(); // CreateVehicleAcceptableBodyRequest | Vehicle object that needs to be added to the platform
apiInstance.modulesVehicleHttpControllersVehicleControllerCreateVehicle(tenant, createVehicleAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createVehicleAcceptableBodyRequest** | [**CreateVehicleAcceptableBodyRequest**](CreateVehicleAcceptableBodyRequest.md)| Vehicle object that needs to be added to the platform | 

### Return type

[**InlineResponse2017**](InlineResponse2017.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerDeleteVehicle

> modulesVehicleHttpControllersVehicleControllerDeleteVehicle(tenant, vid)

Deletes a single vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerDeleteVehicle(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesVehicleHttpControllersVehicleControllerDoorClose

> InlineResponse2001 modulesVehicleHttpControllersVehicleControllerDoorClose(tenant, vid)

Closes the doors of a vehicle

Closes the doors of a vehicle on a compatible device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerDoorClose(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerDoorOpen

> InlineResponse2002 modulesVehicleHttpControllersVehicleControllerDoorOpen(tenant, vid)

Opens the doors of a vehicle

Opens the doors of a vehicle on a compatible device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerDoorOpen(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerFindVehicle

> VehicleModel modulesVehicleHttpControllersVehicleControllerFindVehicle(tenant, vid)

Get a single vehicle

Return a specific vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerFindVehicle(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**VehicleModel**](VehicleModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleDeviceAlerts

> [DeviceAlertDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleDeviceAlerts(tenant, vid, opts)

Get all device alerts based on the imei&#39;s

Returns vehicles data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleDeviceAlerts(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[DeviceAlertDTO]**](DeviceAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleDevices

> [DeviceDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleDevices(tenant, vid, opts)

Get devices for a specific vehicle with time range

Get all devices of a vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'start': null, // Number | 
  'end': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleDevices(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **start** | [**Number**](.md)|  | [optional] 
 **end** | [**Number**](.md)|  | [optional] 

### Return type

[**[DeviceDTO]**](DeviceDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleGeoAlerts

> [GeoAlertDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleGeoAlerts(tenant, vid, opts)

Get all geo alerts based on the imei&#39;s

Returns geo alerts for a specified vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleGeoAlerts(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[GeoAlertDTO]**](GeoAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleIdleEvents

> [IdleEventDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleIdleEvents(tenant, vid, opts)

Get idle events for a specific vehicle

Get all idle events of a vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleIdleEvents(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[IdleEventDTO]**](IdleEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleImpacts

> [ImpactDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleImpacts(tenant, vid, opts)

Get all impacts based on the imei&#39;s

Returns a vehicles impacts data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleImpacts(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[ImpactDTO]**](ImpactDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleJourneys

> [JourneyDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleJourneys(tenant, vid, opts)

Get all journeys of a vehicle

Returns all vehicles journeys

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleJourneys(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[JourneyDTO]**](JourneyDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleMileage

> [VehicleMileage] modulesVehicleHttpControllersVehicleControllerGetVehicleMileage(tenant, registration)

Get mileage for a specific vehicle

Get rental mileage a vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let registration = "registration_example"; // String | Vehicle Registration
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleMileage(tenant, registration, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **registration** | **String**| Vehicle Registration | 

### Return type

[**[VehicleMileage]**](VehicleMileage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleNrms

> [NRMModel] modulesVehicleHttpControllersVehicleControllerGetVehicleNrms(tenant, vid, opts)

Get nrms for a specific vehicle

Return an array of nrms that the specific vehicle has been associated with.

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleNrms(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[NRMModel]**](NRMModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehiclePosition

> PositionDTO modulesVehicleHttpControllersVehicleControllerGetVehiclePosition(tenant, vid)

Get latest position for a specific vehicle

Return latest position for the vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehiclePosition(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**PositionDTO**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleRentals

> [RentalModel] modulesVehicleHttpControllersVehicleControllerGetVehicleRentals(tenant, vid)

Get rentals based on the RA number related with vehicle

Returns rental data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleRentals(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**[RentalModel]**](RentalModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleRepairOrders

> [RepairOrderModel] modulesVehicleHttpControllersVehicleControllerGetVehicleRepairOrders(tenant, vid)

Get all repair orders based on the imei&#39;s

Returns vehicles data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleRepairOrders(tenant, vid, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 

### Return type

[**[RepairOrderModel]**](RepairOrderModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicleSpeedEvents

> [SpeedEventDTO] modulesVehicleHttpControllersVehicleControllerGetVehicleSpeedEvents(tenant, vid, opts)

Get speed events for a specific vehicle

Get all speed events of a vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicleSpeedEvents(tenant, vid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[SpeedEventDTO]**](SpeedEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerGetVehicles

> InlineResponse2007 modulesVehicleHttpControllersVehicleControllerGetVehicles(tenant, opts)

Get all vehicles except by deleted and sold vehicles

Returns vehicles data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rentalType': "rentalType_example", // String | Fleet type to filter by
  'vid': 56, // Number | Vehicle ID to filter by
  'registration': "registration_example", // String | Registration to filter by
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesVehicleHttpControllersVehicleControllerGetVehicles(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalType** | **String**| Fleet type to filter by | [optional] 
 **vid** | **Number**| Vehicle ID to filter by | [optional] 
 **registration** | **String**| Registration to filter by | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesVehicleHttpControllersVehicleControllerUpdateVehicle

> modulesVehicleHttpControllersVehicleControllerUpdateVehicle(tenant, vid, updateVehicleAcceptableBodyRequest)

Update a specific vehicle

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.VehiclesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let vid = "vid_example"; // String | Vehicle ID
let updateVehicleAcceptableBodyRequest = new PlatformApi.UpdateVehicleAcceptableBodyRequest(); // UpdateVehicleAcceptableBodyRequest | Vehicle values to be update
apiInstance.modulesVehicleHttpControllersVehicleControllerUpdateVehicle(tenant, vid, updateVehicleAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **String**| Vehicle ID | 
 **updateVehicleAcceptableBodyRequest** | [**UpdateVehicleAcceptableBodyRequest**](UpdateVehicleAcceptableBodyRequest.md)| Vehicle values to be update | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

