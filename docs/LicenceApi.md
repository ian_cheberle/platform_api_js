# PlatformApi.LicenceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence**](LicenceApi.md#modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence) | **POST** /{tenant}/counterless/renters/{renterId}/licence | Creates a new Licence
[**modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence**](LicenceApi.md#modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence) | **DELETE** /{tenant}/counterless/renters/{renterId}/licence | Deletes a Licence by Renter Id
[**modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence**](LicenceApi.md#modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence) | **GET** /{tenant}/counterless/renters/{renterId}/licence | Gets Renter licence
[**modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences**](LicenceApi.md#modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences) | **GET** /{tenant}/counterless/licences | Get all Licences
[**modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence**](LicenceApi.md#modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence) | **PATCH** /{tenant}/counterless/renters/{renterId}/licence | Update a Renter licence



## modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence

> InlineResponse201 modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence(tenant, renterId, createLicenceAcceptableBodyRequest)

Creates a new Licence

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.LicenceApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter whose data should be updated
let createLicenceAcceptableBodyRequest = new PlatformApi.CreateLicenceAcceptableBodyRequest(); // CreateLicenceAcceptableBodyRequest | Licence object that needs to be added to the platform
apiInstance.modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence(tenant, renterId, createLicenceAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter whose data should be updated | 
 **createLicenceAcceptableBodyRequest** | [**CreateLicenceAcceptableBodyRequest**](CreateLicenceAcceptableBodyRequest.md)| Licence object that needs to be added to the platform | 

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence

> AnyType modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence(tenant, renterId)

Deletes a Licence by Renter Id

Delete a Licence

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.LicenceApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter of the request (Driver Id)
apiInstance.modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence(tenant, renterId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter of the request (Driver Id) | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence

> LicenceModel modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence(tenant, renterId)

Gets Renter licence

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.LicenceApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter Id
apiInstance.modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence(tenant, renterId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter Id | 

### Return type

[**LicenceModel**](LicenceModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences

> AllLicencesPaginated modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences(tenant, opts)

Get all Licences

Returns Licence data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.LicenceApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'driverId': "driverId_example", // String | 
  'licenceApproved': true, // Boolean | 
  'licenceApprovedUser': "licenceApprovedUser_example", // String | 
  'licenceRejected': true, // Boolean | 
  'licenceRejectedUser': "licenceRejectedUser_example", // String | 
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**|  | [optional] 
 **licenceApproved** | **Boolean**|  | [optional] 
 **licenceApprovedUser** | **String**|  | [optional] 
 **licenceRejected** | **Boolean**|  | [optional] 
 **licenceRejectedUser** | **String**|  | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllLicencesPaginated**](AllLicencesPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence

> modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence(tenant, renterId, updateLicenceAcceptableBodyRequest)

Update a Renter licence

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.LicenceApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter whose data should be updated
let updateLicenceAcceptableBodyRequest = new PlatformApi.UpdateLicenceAcceptableBodyRequest(); // UpdateLicenceAcceptableBodyRequest | Licence object that needs to be updated to the platform
apiInstance.modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence(tenant, renterId, updateLicenceAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter whose data should be updated | 
 **updateLicenceAcceptableBodyRequest** | [**UpdateLicenceAcceptableBodyRequest**](UpdateLicenceAcceptableBodyRequest.md)| Licence object that needs to be updated to the platform | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

