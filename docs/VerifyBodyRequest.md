# PlatformApi.VerifyBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**token** | **String** |  | [optional] 


