# PlatformApi.RentalAssociationModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimestamp** | **String** |  | [optional] 
**endTimestamp** | **String** |  | [optional] 
**externalId** | **Number** |  | [optional] 
**rentalAgreement** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsOdometerIn** | **Number** |  | [optional] 
**rsOdometerOut** | **Number** |  | [optional] 
**kmsStart** | **Number** |  | [optional] 
**kmsEnd** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 


