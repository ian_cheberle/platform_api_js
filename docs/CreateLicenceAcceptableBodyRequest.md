# PlatformApi.CreateLicenceAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**licenceIssue** | **String** |  | [optional] 
**licenceExpiry** | **String** |  | [optional] 


