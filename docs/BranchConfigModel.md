# PlatformApi.BranchConfigModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **Number** |  | [optional] 
**lat** | **Number** |  | [optional] 
**lon** | **Number** |  | [optional] 
**directions** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**emailListLicenceApproval** | **[String]** |  | [optional] 
**emailListCheckIn** | **[String]** |  | [optional] 
**emailListKeySwap** | **[String]** |  | [optional] 


