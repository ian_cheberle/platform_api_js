# PlatformApi.IdleEventDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**duration** | **Number** |  | [optional] 
**latitude** | **Number** |  | [optional] 
**longitude** | **Number** |  | [optional] 
**imei** | **Number** |  | [optional] 


