# PlatformApi.NRMsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesNRMHttpControllersNRMControllerCreateNRM**](NRMsApi.md#modulesNRMHttpControllersNRMControllerCreateNRM) | **POST** /{tenant}/nrms | Creates a single NRM
[**modulesNRMHttpControllersNRMControllerDeleteNRM**](NRMsApi.md#modulesNRMHttpControllersNRMControllerDeleteNRM) | **DELETE** /{tenant}/nrms/{nrm_number} | Deletes a single NRM
[**modulesNRMHttpControllersNRMControllerGetAllNrms**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetAllNrms) | **GET** /{tenant}/nrms | Get all NRMs
[**modulesNRMHttpControllersNRMControllerGetDeviceAlerts**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetDeviceAlerts) | **GET** /{tenant}/nrms/{nrm_number}/devicealerts | Get device alerts for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetDeviceIdleEvents**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetDeviceIdleEvents) | **GET** /{tenant}/nrms/{nrm_number}/idleevents | Get device idle events for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetDeviceImpacts**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetDeviceImpacts) | **GET** /{tenant}/nrms/{nrm_number}/impacts | Get device impacts for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetDevicePosition**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetDevicePosition) | **GET** /{tenant}/nrms/{nrm_number}/position | Get device position for an specific NRM
[**modulesNRMHttpControllersNRMControllerGetDeviceSpeedEvents**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetDeviceSpeedEvents) | **GET** /{tenant}/nrms/{nrm_number}/speedevents | Get device speed events for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetGeoAlerts**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetGeoAlerts) | **GET** /{tenant}/nrms/{nrm_number}/geoalerts | Get geo alerts for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetJourneys**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetJourneys) | **GET** /{tenant}/nrms/{nrm_number}/journeys | Get journeys for a specific NRM
[**modulesNRMHttpControllersNRMControllerGetNRM**](NRMsApi.md#modulesNRMHttpControllersNRMControllerGetNRM) | **GET** /{tenant}/nrms/{nrm_number} | Gets an NRM based on the nrm_number
[**modulesNRMHttpControllersNRMControllerUpdateNRM**](NRMsApi.md#modulesNRMHttpControllersNRMControllerUpdateNRM) | **PATCH** /{tenant}/nrms/{nrm_number} | Update an specific NRM



## modulesNRMHttpControllersNRMControllerCreateNRM

> InlineResponse2012 modulesNRMHttpControllersNRMControllerCreateNRM(tenant, createNRMAcceptableBodyRequest)

Creates a single NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createNRMAcceptableBodyRequest = new PlatformApi.CreateNRMAcceptableBodyRequest(); // CreateNRMAcceptableBodyRequest | NRM object that needs to be added to the platform
apiInstance.modulesNRMHttpControllersNRMControllerCreateNRM(tenant, createNRMAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createNRMAcceptableBodyRequest** | [**CreateNRMAcceptableBodyRequest**](CreateNRMAcceptableBodyRequest.md)| NRM object that needs to be added to the platform | 

### Return type

[**InlineResponse2012**](InlineResponse2012.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerDeleteNRM

> modulesNRMHttpControllersNRMControllerDeleteNRM(tenant, nrmNumber)

Deletes a single NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM number
apiInstance.modulesNRMHttpControllersNRMControllerDeleteNRM(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM number | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesNRMHttpControllersNRMControllerGetAllNrms

> AllNRMsPaginated modulesNRMHttpControllersNRMControllerGetAllNrms(tenant, opts)

Get all NRMs

Returns nrm data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'currentLocation': "currentLocation_example" // String | 
};
apiInstance.modulesNRMHttpControllersNRMControllerGetAllNrms(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **currentLocation** | **String**|  | [optional] 

### Return type

[**AllNRMsPaginated**](AllNRMsPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetDeviceAlerts

> [DeviceAlertDTO] modulesNRMHttpControllersNRMControllerGetDeviceAlerts(tenant, nrmNumber)

Get device alerts for a specific NRM

Get all device alerts of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetDeviceAlerts(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[DeviceAlertDTO]**](DeviceAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetDeviceIdleEvents

> [IdleEventDTO] modulesNRMHttpControllersNRMControllerGetDeviceIdleEvents(tenant, nrmNumber)

Get device idle events for a specific NRM

Get all device idle events of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetDeviceIdleEvents(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[IdleEventDTO]**](IdleEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetDeviceImpacts

> [ImpactDTO] modulesNRMHttpControllersNRMControllerGetDeviceImpacts(tenant, nrmNumber)

Get device impacts for a specific NRM

Get all device impact of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetDeviceImpacts(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[ImpactDTO]**](ImpactDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetDevicePosition

> [PositionDTO] modulesNRMHttpControllersNRMControllerGetDevicePosition(tenant, nrmNumber)

Get device position for an specific NRM

Get the current position for a NRM if have not finished

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetDevicePosition(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[PositionDTO]**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetDeviceSpeedEvents

> [SpeedEventDTO] modulesNRMHttpControllersNRMControllerGetDeviceSpeedEvents(tenant, nrmNumber)

Get device speed events for a specific NRM

Get all device speed events of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetDeviceSpeedEvents(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[SpeedEventDTO]**](SpeedEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetGeoAlerts

> [GeoAlertDTO] modulesNRMHttpControllersNRMControllerGetGeoAlerts(tenant, nrmNumber)

Get geo alerts for a specific NRM

Get all geo alerts of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetGeoAlerts(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[GeoAlertDTO]**](GeoAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetJourneys

> [JourneyDTO] modulesNRMHttpControllersNRMControllerGetJourneys(tenant, nrmNumber)

Get journeys for a specific NRM

Get all journeys of a NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM Number
apiInstance.modulesNRMHttpControllersNRMControllerGetJourneys(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM Number | 

### Return type

[**[JourneyDTO]**](JourneyDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerGetNRM

> NRMModel modulesNRMHttpControllersNRMControllerGetNRM(tenant, nrmNumber)

Gets an NRM based on the nrm_number

Returns a nrm

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | 
apiInstance.modulesNRMHttpControllersNRMControllerGetNRM(tenant, nrmNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**|  | 

### Return type

[**NRMModel**](NRMModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesNRMHttpControllersNRMControllerUpdateNRM

> modulesNRMHttpControllersNRMControllerUpdateNRM(tenant, nrmNumber, updateNRMAcceptableBodyRequest)

Update an specific NRM

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.NRMsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let nrmNumber = "nrmNumber_example"; // String | NRM number
let updateNRMAcceptableBodyRequest = new PlatformApi.UpdateNRMAcceptableBodyRequest(); // UpdateNRMAcceptableBodyRequest | NRM values to be updated
apiInstance.modulesNRMHttpControllersNRMControllerUpdateNRM(tenant, nrmNumber, updateNRMAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **nrmNumber** | **String**| NRM number | 
 **updateNRMAcceptableBodyRequest** | [**UpdateNRMAcceptableBodyRequest**](UpdateNRMAcceptableBodyRequest.md)| NRM values to be updated | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

