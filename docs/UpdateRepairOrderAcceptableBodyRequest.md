# PlatformApi.UpdateRepairOrderAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endTimestamp** | **String** |  | [optional] 
**startTimestamp** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsOdometerIn** | **Number** |  | [optional] 
**rsOdometerOut** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**summary** | **String** |  | [optional] 


