# PlatformApi.InlineResponse422

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** | The error message | [optional] 


