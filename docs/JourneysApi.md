# PlatformApi.JourneysApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesJourneyHttpControllersJourneyControllerGetJourneys**](JourneysApi.md#modulesJourneyHttpControllersJourneyControllerGetJourneys) | **GET** /{tenant}/journeys | Get all journeys



## modulesJourneyHttpControllersJourneyControllerGetJourneys

> AllJourneysPaginated modulesJourneyHttpControllersJourneyControllerGetJourneys(tenant, opts)

Get all journeys

Returns journeys data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.JourneysApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'vid': 56, // Number | Vehicle ID to filter by
  'registration': "registration_example", // String | Registration to filter by
  'imei': "imei_example", // String | Imei to filter by
  'raNumber': "raNumber_example", // String | Rental Agreement Number to filter by
  'nrmNumber': "nrmNumber_example", // String | NRM Number to filter by
  'repairNumber': "repairNumber_example", // String | Repair Order Number to filter by
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesJourneyHttpControllersJourneyControllerGetJourneys(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **vid** | **Number**| Vehicle ID to filter by | [optional] 
 **registration** | **String**| Registration to filter by | [optional] 
 **imei** | **String**| Imei to filter by | [optional] 
 **raNumber** | **String**| Rental Agreement Number to filter by | [optional] 
 **nrmNumber** | **String**| NRM Number to filter by | [optional] 
 **repairNumber** | **String**| Repair Order Number to filter by | [optional] 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllJourneysPaginated**](AllJourneysPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

