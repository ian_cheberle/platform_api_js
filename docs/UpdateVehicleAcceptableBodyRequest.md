# PlatformApi.UpdateVehicleAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | [optional] 
**colour** | **String** |  | [optional] 
**currentKms** | **Number** |  | [optional] 
**dateServiceDue** | **String** |  | [optional] 
**dueOffDate** | **String** |  | [optional] 
**dueOffKms** | **Number** |  | [optional] 
**fleetType** | **String** |  | [optional] 
**lastMovementDate** | **String** |  | [optional] 
**make** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rentalStation** | **String** |  | [optional] 
**salesDate** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**supplier** | **String** |  | [optional] 
**unitnr** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**vid** | **String** |  | [optional] 


