# PlatformApi.DeviceAlertDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**imei** | **Number** |  | [optional] 
**timestamp** | **Number** |  | [optional] 
**type** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**vid** | **String** |  | [optional] 


