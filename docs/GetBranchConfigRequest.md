# PlatformApi.GetBranchConfigRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**perPage** | **Number** |  | [optional] 
**page** | **Number** |  | [optional] 


