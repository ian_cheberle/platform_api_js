# PlatformApi.RenterUserModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**driverId** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**emailVerifiedAt** | **String** |  | [optional] 
**accountType** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**dob** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 


