# PlatformApi.PhotosApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesPhotoHttpControllersPhotoControllerCreatePhoto**](PhotosApi.md#modulesPhotoHttpControllersPhotoControllerCreatePhoto) | **POST** /{tenant}/counterless/photos | Creates a single photo
[**modulesPhotoHttpControllersPhotoControllerDeletePhoto**](PhotosApi.md#modulesPhotoHttpControllersPhotoControllerDeletePhoto) | **DELETE** /{tenant}/counterless/photos/{photo_id} | Deletes a single Photo
[**modulesPhotoHttpControllersPhotoControllerGetPhoto**](PhotosApi.md#modulesPhotoHttpControllersPhotoControllerGetPhoto) | **GET** /{tenant}/counterless/photos/{photo_id} | Finds a single photo
[**modulesPhotoHttpControllersPhotoControllerGetPhotos**](PhotosApi.md#modulesPhotoHttpControllersPhotoControllerGetPhotos) | **GET** /{tenant}/counterless/photos | Get all Photos
[**modulesPhotoHttpControllersPhotoControllerUpdatePhoto**](PhotosApi.md#modulesPhotoHttpControllersPhotoControllerUpdatePhoto) | **PATCH** /{tenant}/counterless/photos/{photo_id} | Update an specific Photo



## modulesPhotoHttpControllersPhotoControllerCreatePhoto

> InlineResponse201 modulesPhotoHttpControllersPhotoControllerCreatePhoto(tenant, createPhotoAcceptableBodyRequest)

Creates a single photo

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.PhotosApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createPhotoAcceptableBodyRequest = new PlatformApi.CreatePhotoAcceptableBodyRequest(); // CreatePhotoAcceptableBodyRequest | Photo object that needs to be added to the platform
apiInstance.modulesPhotoHttpControllersPhotoControllerCreatePhoto(tenant, createPhotoAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createPhotoAcceptableBodyRequest** | [**CreatePhotoAcceptableBodyRequest**](CreatePhotoAcceptableBodyRequest.md)| Photo object that needs to be added to the platform | 

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesPhotoHttpControllersPhotoControllerDeletePhoto

> modulesPhotoHttpControllersPhotoControllerDeletePhoto(tenant, photoId)

Deletes a single Photo

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.PhotosApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let photoId = "photoId_example"; // String | Photo Id
apiInstance.modulesPhotoHttpControllersPhotoControllerDeletePhoto(tenant, photoId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **photoId** | **String**| Photo Id | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesPhotoHttpControllersPhotoControllerGetPhoto

> PhotoModel modulesPhotoHttpControllersPhotoControllerGetPhoto(tenant, photoId)

Finds a single photo

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.PhotosApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let photoId = 56; // Number | Photo Id
apiInstance.modulesPhotoHttpControllersPhotoControllerGetPhoto(tenant, photoId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **photoId** | **Number**| Photo Id | 

### Return type

[**PhotoModel**](PhotoModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesPhotoHttpControllersPhotoControllerGetPhotos

> AllPhotosPaginated modulesPhotoHttpControllersPhotoControllerGetPhotos(tenant, opts)

Get all Photos

Returns photos data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.PhotosApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'driverId': "driverId_example", // String | 
  'licenceId': "licenceId_example", // String | 
  'rentalId': "rentalId_example", // String | 
  'type': "type_example", // String | 
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesPhotoHttpControllersPhotoControllerGetPhotos(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**|  | [optional] 
 **licenceId** | **String**|  | [optional] 
 **rentalId** | **String**|  | [optional] 
 **type** | **String**|  | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllPhotosPaginated**](AllPhotosPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesPhotoHttpControllersPhotoControllerUpdatePhoto

> AnyType modulesPhotoHttpControllersPhotoControllerUpdatePhoto(tenant, photoId, updatePhotoAcceptableBodyRequest)

Update an specific Photo

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.PhotosApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let photoId = 56; // Number | Photo Id
let updatePhotoAcceptableBodyRequest = new PlatformApi.UpdatePhotoAcceptableBodyRequest(); // UpdatePhotoAcceptableBodyRequest | Photo values to be updated
apiInstance.modulesPhotoHttpControllersPhotoControllerUpdatePhoto(tenant, photoId, updatePhotoAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **photoId** | **Number**| Photo Id | 
 **updatePhotoAcceptableBodyRequest** | [**UpdatePhotoAcceptableBodyRequest**](UpdatePhotoAcceptableBodyRequest.md)| Photo values to be updated | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

