# PlatformApi.VehicleCounterlessModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | [optional] 
**colour** | **String** |  | [optional] 
**make** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**unitnr** | **String** |  | [optional] 
**vid** | **String** |  | [optional] 


