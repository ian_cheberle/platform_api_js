# PlatformApi.SpeedEventDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 
**imei** | **Number** |  | [optional] 
**journeyId** | **String** |  | [optional] 
**maxSpeedReached** | **Number** |  | [optional] 
**nrmNumber** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**repairNumber** | **Number** |  | [optional] 
**speedLimit** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 


