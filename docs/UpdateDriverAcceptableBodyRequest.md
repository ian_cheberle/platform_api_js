# PlatformApi.UpdateDriverAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dob** | **Date** |  | [optional] 
**email** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**nationality** | **String** |  | [optional] 
**occupation** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**safetyCompetition** | **Boolean** |  | [optional] 
**emailCustomerUpdates** | **Boolean** |  | [optional] 


