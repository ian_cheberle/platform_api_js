# PlatformApi.UpdateRentalAssociationAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**epmStart** | **String** |  | [optional] 
**epmEnd** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsKmIn** | **Number** |  | [optional] 
**rsKmOut** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 


