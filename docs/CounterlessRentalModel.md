# PlatformApi.CounterlessRentalModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**raNumber** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**start** | **Number** |  | [optional] 
**end** | **Number** |  | [optional] 
**stationStart** | **String** |  | [optional] 
**stationEnd** | **String** |  | [optional] 
**stationStartDisplayName** | **String** |  | [optional] 
**stationStartId** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**counterlessStatus** | **String** |  | [optional] 
**checkinAvailableFrom** | **Number** |  | [optional] 
**checkinAvailableUserNotified** | **Boolean** |  | [optional] 
**checkinClosedAfter** | **Boolean** |  | [optional] 
**checkedIn** | **Boolean** |  | [optional] 
**checkedInTime** | **Number** |  | [optional] 
**vehicleAssigned** | **Boolean** |  | [optional] 
**vehicleAssignedRegistration** | **String** |  | [optional] 
**atVehicle** | **Boolean** |  | [optional] 
**atVehicleTime** | **Number** |  | [optional] 
**keySwapActioned** | **Boolean** |  | [optional] 



## Enum: StatusEnum


* `OPEN` (value: `"OPEN"`)

* `CLOSED` (value: `"CLOSED"`)

* `PRECLOSED` (value: `"PRECLOSED"`)

* `CANCELLED` (value: `"CANCELLED"`)





## Enum: CounterlessStatusEnum


* `BOOKED` (value: `"BOOKED"`)

* `CHECKEDIN` (value: `"CHECKEDIN"`)

* `ARRIVED` (value: `"ARRIVED"`)

* `ACTIVE` (value: `"ACTIVE"`)

* `OVERDUE` (value: `"OVERDUE"`)

* `CANCELLED` (value: `"CANCELLED"`)




