# PlatformApi.RentalSummaryModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationOut** | **String** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**imeis** | **String** |  | [optional] 
**odometers** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**registrations** | **String** |  | [optional] 
**rentalStatus** | **String** |  | [optional] 
**vids** | **String** |  | [optional] 


