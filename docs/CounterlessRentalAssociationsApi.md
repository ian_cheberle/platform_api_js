# PlatformApi.CounterlessRentalAssociationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociation**](CounterlessRentalAssociationsApi.md#modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociation) | **GET** /{tenant}/counterless/rentalassociations/{rental_association_id} | Get a counterless Rental Associations record by ID
[**modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociations**](CounterlessRentalAssociationsApi.md#modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociations) | **GET** /{tenant}/counterless/rentalassociations | Get all counterless paginated Rental Associations
[**modulesRentalHttpControllersCounterlessRentalAssociationControllerGetVehicle**](CounterlessRentalAssociationsApi.md#modulesRentalHttpControllersCounterlessRentalAssociationControllerGetVehicle) | **GET** /{tenant}/counterless/rentalassociations/{rental_association_id}/vehicle | Get a counterless Vehicle by Rental Associations ID



## modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociation

> CounterlessRentalAssociationModel modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociation(tenant, rentalAssociationId)

Get a counterless Rental Associations record by ID

Returns a single record of counterless Rental Association by ID

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalAssociationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let rentalAssociationId = 56; // Number | The Rental Association Id
apiInstance.modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociation(tenant, rentalAssociationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalAssociationId** | **Number**| The Rental Association Id | 

### Return type

[**CounterlessRentalAssociationModel**](CounterlessRentalAssociationModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociations

> Pagination modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociations(tenant, opts)

Get all counterless paginated Rental Associations

Returns all paginated counterless Rental Association data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalAssociationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'perPage': 56, // Number | Number of items per page
  'raNumber': "raNumber_example", // String | The RA Number of the associations
  'page': 56 // Number | Current page
};
apiInstance.modulesRentalHttpControllersCounterlessRentalAssociationControllerGetAssociations(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **perPage** | **Number**| Number of items per page | [optional] 
 **raNumber** | **String**| The RA Number of the associations | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**Pagination**](Pagination.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersCounterlessRentalAssociationControllerGetVehicle

> VehicleCounterlessModel modulesRentalHttpControllersCounterlessRentalAssociationControllerGetVehicle(tenant, rentalAssociationId)

Get a counterless Vehicle by Rental Associations ID

Returns a single record of counterless Vehicle by Rental Association ID

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.CounterlessRentalAssociationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let rentalAssociationId = 56; // Number | The Rental Association Id
apiInstance.modulesRentalHttpControllersCounterlessRentalAssociationControllerGetVehicle(tenant, rentalAssociationId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalAssociationId** | **Number**| The Rental Association Id | 

### Return type

[**VehicleCounterlessModel**](VehicleCounterlessModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

