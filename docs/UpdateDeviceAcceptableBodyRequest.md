# PlatformApi.UpdateDeviceAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msisdn** | **Number** |  | [optional] 
**iccid** | **Number** |  | [optional] 
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**network** | **String** |  | [optional] 
**purchasedDate** | **Number** |  | [optional] 
**mxp** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 


