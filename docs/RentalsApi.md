# PlatformApi.RentalsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRentalHttpControllersRentalControllerCreateRental**](RentalsApi.md#modulesRentalHttpControllersRentalControllerCreateRental) | **POST** /{tenant}/rentals | Creates a single rental
[**modulesRentalHttpControllersRentalControllerCreateRentalAssociation**](RentalsApi.md#modulesRentalHttpControllersRentalControllerCreateRentalAssociation) | **POST** /{tenant}/rentals/{ra_number}/associations | Associate a vehicle to a rental
[**modulesRentalHttpControllersRentalControllerCreateRentalComplete**](RentalsApi.md#modulesRentalHttpControllersRentalControllerCreateRentalComplete) | **POST** /{tenant}/rentals/complete | Create a new rental, rental association, vehicle, driver. If any element already exits, it will be updated
[**modulesRentalHttpControllersRentalControllerCreateRentalExtras**](RentalsApi.md#modulesRentalHttpControllersRentalControllerCreateRentalExtras) | **POST** /{tenant}/rentals/{ra_number}/extras | Add a extra to a rental
[**modulesRentalHttpControllersRentalControllerDeleteRental**](RentalsApi.md#modulesRentalHttpControllersRentalControllerDeleteRental) | **DELETE** /{tenant}/rentals/{ra_number} | Deletes a rental
[**modulesRentalHttpControllersRentalControllerDeleteRentalAssociation**](RentalsApi.md#modulesRentalHttpControllersRentalControllerDeleteRentalAssociation) | **DELETE** /{tenant}/rentals/{ra_number}/associations/{external_id} | Deletes a rental association
[**modulesRentalHttpControllersRentalControllerDeleteRentalExtra**](RentalsApi.md#modulesRentalHttpControllersRentalControllerDeleteRentalExtra) | **DELETE** /{tenant}/rentals/{ra_number}/extras/{id} | Deletes a rental extra
[**modulesRentalHttpControllersRentalControllerGetDeviceAlerts**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetDeviceAlerts) | **GET** /{tenant}/rentals/{ra_number}/devicealerts | Get device alerts for a specific rental
[**modulesRentalHttpControllersRentalControllerGetDriver**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetDriver) | **GET** /{tenant}/rentals/{ra_number}/driver | Get driver of a specific rental
[**modulesRentalHttpControllersRentalControllerGetJourneys**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetJourneys) | **GET** /{tenant}/rentals/{ra_number}/journeys | Get journeys for a specific rental
[**modulesRentalHttpControllersRentalControllerGetPosition**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetPosition) | **GET** /{tenant}/rentals/{ra_number}/position | Get latest position of a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalAssociations**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalAssociations) | **GET** /{tenant}/rentals/{ra_number}/associations | Get associations of a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalExtras**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalExtras) | **GET** /{tenant}/rentals/{ra_number}/extras | Get extras for a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalForRANumber**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalForRANumber) | **GET** /{tenant}/rentals/{ra_number} | Finds a single rental
[**modulesRentalHttpControllersRentalControllerGetRentalGeoAlerts**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalGeoAlerts) | **GET** /{tenant}/rentals/{ra_number}/geoalerts | Get geoalerts for a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalIdleEvents**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalIdleEvents) | **GET** /{tenant}/rentals/{ra_number}/idleevents | Get idle events for a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalImpacts**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalImpacts) | **GET** /{tenant}/rentals/{ra_number}/impacts | Get all impacts based on a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalMileage**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalMileage) | **GET** /{tenant}/rentals/{ra_number}/mileage | Get mileage for a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentalSpeedEvents**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalSpeedEvents) | **GET** /{tenant}/rentals/{ra_number}/speedevents | Get speed events for a specific rental
[**modulesRentalHttpControllersRentalControllerGetRentals**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentals) | **GET** /{tenant}/rentals | Get paginated rentals
[**modulesRentalHttpControllersRentalControllerGetRentalsSummary**](RentalsApi.md#modulesRentalHttpControllersRentalControllerGetRentalsSummary) | **GET** /{tenant}/rentals/summary | Get paginated rental summary
[**modulesRentalHttpControllersRentalControllerUpdateRental**](RentalsApi.md#modulesRentalHttpControllersRentalControllerUpdateRental) | **PATCH** /{tenant}/rentals/{ra_number} | Update an specific Rental
[**modulesRentalHttpControllersRentalControllerUpdateRentalAssociation**](RentalsApi.md#modulesRentalHttpControllersRentalControllerUpdateRentalAssociation) | **PATCH** /{tenant}/rentals/{ra_number}/associations/{external_id} | Update a specific rental association



## modulesRentalHttpControllersRentalControllerCreateRental

> InlineResponse2013 modulesRentalHttpControllersRentalControllerCreateRental(tenant, createRentalAcceptableBodyRequest)

Creates a single rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createRentalAcceptableBodyRequest = new PlatformApi.CreateRentalAcceptableBodyRequest(); // CreateRentalAcceptableBodyRequest | Rental object that needs to be added to the platform
apiInstance.modulesRentalHttpControllersRentalControllerCreateRental(tenant, createRentalAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createRentalAcceptableBodyRequest** | [**CreateRentalAcceptableBodyRequest**](CreateRentalAcceptableBodyRequest.md)| Rental object that needs to be added to the platform | 

### Return type

[**InlineResponse2013**](InlineResponse2013.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerCreateRentalAssociation

> InlineResponse201 modulesRentalHttpControllersRentalControllerCreateRentalAssociation(tenant, raNumber, createRentalAssociationAcceptableBodyRequest)

Associate a vehicle to a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement Number
let createRentalAssociationAcceptableBodyRequest = new PlatformApi.CreateRentalAssociationAcceptableBodyRequest(); // CreateRentalAssociationAcceptableBodyRequest | Association object that needs to be added to the platform
apiInstance.modulesRentalHttpControllersRentalControllerCreateRentalAssociation(tenant, raNumber, createRentalAssociationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement Number | 
 **createRentalAssociationAcceptableBodyRequest** | [**CreateRentalAssociationAcceptableBodyRequest**](CreateRentalAssociationAcceptableBodyRequest.md)| Association object that needs to be added to the platform | 

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerCreateRentalComplete

> InlineResponse2003 modulesRentalHttpControllersRentalControllerCreateRentalComplete(tenant, createRentalCompleteAcceptableBodyRequest)

Create a new rental, rental association, vehicle, driver. If any element already exits, it will be updated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createRentalCompleteAcceptableBodyRequest = new PlatformApi.CreateRentalCompleteAcceptableBodyRequest(); // CreateRentalCompleteAcceptableBodyRequest | Rental, Driver, Vehicle and association be created/updated
apiInstance.modulesRentalHttpControllersRentalControllerCreateRentalComplete(tenant, createRentalCompleteAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createRentalCompleteAcceptableBodyRequest** | [**CreateRentalCompleteAcceptableBodyRequest**](CreateRentalCompleteAcceptableBodyRequest.md)| Rental, Driver, Vehicle and association be created/updated | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerCreateRentalExtras

> InlineResponse201 modulesRentalHttpControllersRentalControllerCreateRentalExtras(tenant, raNumber, createRentalExtraAcceptableBodyRequest)

Add a extra to a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement Number
let createRentalExtraAcceptableBodyRequest = new PlatformApi.CreateRentalExtraAcceptableBodyRequest(); // CreateRentalExtraAcceptableBodyRequest | Extra object that needs to be added to the platform
apiInstance.modulesRentalHttpControllersRentalControllerCreateRentalExtras(tenant, raNumber, createRentalExtraAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement Number | 
 **createRentalExtraAcceptableBodyRequest** | [**CreateRentalExtraAcceptableBodyRequest**](CreateRentalExtraAcceptableBodyRequest.md)| Extra object that needs to be added to the platform | 

### Return type

[**InlineResponse201**](InlineResponse201.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerDeleteRental

> AnyType modulesRentalHttpControllersRentalControllerDeleteRental(tenant, raNumber)

Deletes a rental

Delete a specific rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
apiInstance.modulesRentalHttpControllersRentalControllerDeleteRental(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerDeleteRentalAssociation

> AnyType modulesRentalHttpControllersRentalControllerDeleteRentalAssociation(tenant, raNumber, externalId)

Deletes a rental association

Delete a specific rental association

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
let externalId = "externalId_example"; // String | Client ID on the tenant system
apiInstance.modulesRentalHttpControllersRentalControllerDeleteRentalAssociation(tenant, raNumber, externalId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 
 **externalId** | **String**| Client ID on the tenant system | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerDeleteRentalExtra

> AnyType modulesRentalHttpControllersRentalControllerDeleteRentalExtra(tenant, raNumber, id)

Deletes a rental extra

Delete a rental extra from a specific Rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
let id = "id_example"; // String | Extra id
apiInstance.modulesRentalHttpControllersRentalControllerDeleteRentalExtra(tenant, raNumber, id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 
 **id** | **String**| Extra id | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetDeviceAlerts

> [DeviceAlertDTO] modulesRentalHttpControllersRentalControllerGetDeviceAlerts(tenant, raNumber)

Get device alerts for a specific rental

Get all device alerts of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetDeviceAlerts(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[DeviceAlertDTO]**](DeviceAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetDriver

> DriverModel modulesRentalHttpControllersRentalControllerGetDriver(tenant, raNumber)

Get driver of a specific rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement Number
apiInstance.modulesRentalHttpControllersRentalControllerGetDriver(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement Number | 

### Return type

[**DriverModel**](DriverModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetJourneys

> [JourneyDTO] modulesRentalHttpControllersRentalControllerGetJourneys(tenant, raNumber)

Get journeys for a specific rental

Get all journeys of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetJourneys(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[JourneyDTO]**](JourneyDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetPosition

> [PositionDTO] modulesRentalHttpControllersRentalControllerGetPosition(tenant, raNumber)

Get latest position of a specific rental

Returns latest postion data for a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetPosition(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[PositionDTO]**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalAssociations

> [RentalAssociationModel] modulesRentalHttpControllersRentalControllerGetRentalAssociations(tenant, raNumber)

Get associations of a specific rental

Get associations of a specific rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalAssociations(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[RentalAssociationModel]**](RentalAssociationModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalExtras

> [RentalExtraModel] modulesRentalHttpControllersRentalControllerGetRentalExtras(tenant, raNumber)

Get extras for a specific rental

Get all extras of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalExtras(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[RentalExtraModel]**](RentalExtraModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalForRANumber

> RentalModel modulesRentalHttpControllersRentalControllerGetRentalForRANumber(tenant, raNumber)

Finds a single rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement Number
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalForRANumber(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement Number | 

### Return type

[**RentalModel**](RentalModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalGeoAlerts

> [GeoAlertDTO] modulesRentalHttpControllersRentalControllerGetRentalGeoAlerts(tenant, raNumber, opts)

Get geoalerts for a specific rental

Get all geoalerts of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalGeoAlerts(tenant, raNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[GeoAlertDTO]**](GeoAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalIdleEvents

> [IdleEventDTO] modulesRentalHttpControllersRentalControllerGetRentalIdleEvents(tenant, raNumber)

Get idle events for a specific rental

Get all idle events of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalIdleEvents(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[IdleEventDTO]**](IdleEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalImpacts

> [ImpactDTO] modulesRentalHttpControllersRentalControllerGetRentalImpacts(tenant, raNumber, opts)

Get all impacts based on a specific rental

Returns a rental impacts data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalImpacts(tenant, raNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[ImpactDTO]**](ImpactDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalMileage

> [RentalMileage] modulesRentalHttpControllersRentalControllerGetRentalMileage(tenant, raNumber)

Get mileage for a specific rental

Get all mileage of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalMileage(tenant, raNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 

### Return type

[**[RentalMileage]**](RentalMileage.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalSpeedEvents

> [SpeedEventDTO] modulesRentalHttpControllersRentalControllerGetRentalSpeedEvents(tenant, raNumber, opts)

Get speed events for a specific rental

Get all speed events of a rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Number
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalSpeedEvents(tenant, raNumber, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Number | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[SpeedEventDTO]**](SpeedEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentals

> AllRentalsPaginated modulesRentalHttpControllersRentalControllerGetRentals(tenant, opts)

Get paginated rentals

Returns rental data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rentalStatus': ["null"], // [String] | 
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'currentLocation': "currentLocation_example", // String | 
  'longTerm': ["null"], // [String] | 
  'raNumber': "raNumber_example", // String | Rental Agreement Number to filter by
  'vid': 56, // Number | Vehicle ID to filter by
  'registration': "registration_example", // String | Registration to filter by
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRentalHttpControllersRentalControllerGetRentals(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalStatus** | [**[String]**](String.md)|  | [optional] 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **currentLocation** | **String**|  | [optional] 
 **longTerm** | [**[String]**](String.md)|  | [optional] 
 **raNumber** | **String**| Rental Agreement Number to filter by | [optional] 
 **vid** | **Number**| Vehicle ID to filter by | [optional] 
 **registration** | **String**| Registration to filter by | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**AllRentalsPaginated**](AllRentalsPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerGetRentalsSummary

> RentalsSummaryPaginated modulesRentalHttpControllersRentalControllerGetRentalsSummary(tenant, opts)

Get paginated rental summary

Returns rental data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rentalStatus': ["null"], // [String] | 
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'currentLocation': "currentLocation_example", // String | 
  'longTerm': ["null"], // [String] | 
  'raNumber': "raNumber_example", // String | Rental Agreement Number to filter by
  'vid': 56, // Number | Vehicle ID to filter by
  'registration': "registration_example", // String | Registration to filter by
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRentalHttpControllersRentalControllerGetRentalsSummary(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rentalStatus** | [**[String]**](String.md)|  | [optional] 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **currentLocation** | **String**|  | [optional] 
 **longTerm** | [**[String]**](String.md)|  | [optional] 
 **raNumber** | **String**| Rental Agreement Number to filter by | [optional] 
 **vid** | **Number**| Vehicle ID to filter by | [optional] 
 **registration** | **String**| Registration to filter by | [optional] 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**RentalsSummaryPaginated**](RentalsSummaryPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerUpdateRental

> AnyType modulesRentalHttpControllersRentalControllerUpdateRental(tenant, raNumber, updateRentalAcceptableBodyRequest)

Update an specific Rental

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
let updateRentalAcceptableBodyRequest = new PlatformApi.UpdateRentalAcceptableBodyRequest(); // UpdateRentalAcceptableBodyRequest | Rental values to be updated
apiInstance.modulesRentalHttpControllersRentalControllerUpdateRental(tenant, raNumber, updateRentalAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 
 **updateRentalAcceptableBodyRequest** | [**UpdateRentalAcceptableBodyRequest**](UpdateRentalAcceptableBodyRequest.md)| Rental values to be updated | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRentalHttpControllersRentalControllerUpdateRentalAssociation

> modulesRentalHttpControllersRentalControllerUpdateRentalAssociation(tenant, raNumber, externalId, updateRentalAssociationAcceptableBodyRequest)

Update a specific rental association

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentalsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let raNumber = "raNumber_example"; // String | Rental Agreement id
let externalId = "externalId_example"; // String | Client ID on the tenant system
let updateRentalAssociationAcceptableBodyRequest = new PlatformApi.UpdateRentalAssociationAcceptableBodyRequest(); // UpdateRentalAssociationAcceptableBodyRequest | Rental Association values to be updated
apiInstance.modulesRentalHttpControllersRentalControllerUpdateRentalAssociation(tenant, raNumber, externalId, updateRentalAssociationAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **raNumber** | **String**| Rental Agreement id | 
 **externalId** | **String**| Client ID on the tenant system | 
 **updateRentalAssociationAcceptableBodyRequest** | [**UpdateRentalAssociationAcceptableBodyRequest**](UpdateRentalAssociationAcceptableBodyRequest.md)| Rental Association values to be updated | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

