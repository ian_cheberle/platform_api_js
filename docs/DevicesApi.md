# PlatformApi.DevicesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesDeviceHttpControllersDeviceControllerCreateDevice**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerCreateDevice) | **POST** /{tenant}/devices | Creates a device
[**modulesDeviceHttpControllersDeviceControllerDeleteDevice**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerDeleteDevice) | **DELETE** /{tenant}/devices/{imei} | Deletes a device
[**modulesDeviceHttpControllersDeviceControllerGetDevice**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDevice) | **GET** /{tenant}/devices/{imei} | Get a single device information
[**modulesDeviceHttpControllersDeviceControllerGetDeviceAlerts**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceAlerts) | **GET** /{tenant}/devices/{imei}/devicealerts | Get all alerts for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceGeoAlerts**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceGeoAlerts) | **GET** /{tenant}/devices/{imei}/geoalerts | Get all geo alerts for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceIdleEvents**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceIdleEvents) | **GET** /{tenant}/devices/{imei}/idleevents | Get all idle events for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceImpacts**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceImpacts) | **GET** /{tenant}/devices/{imei}/impacts | Get all impacts for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceJourneys**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceJourneys) | **GET** /{tenant}/devices/{imei}/journeys | Get all journeys for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceRentals**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceRentals) | **GET** /{tenant}/devices/{imei}/rentals | Get all rentals for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceRepairOrders**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceRepairOrders) | **GET** /{tenant}/devices/{imei}/repairorders | Get all repair orders for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDeviceSpeedEvents**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDeviceSpeedEvents) | **GET** /{tenant}/devices/{imei}/speedevents | Get all Speed events for a specific device
[**modulesDeviceHttpControllersDeviceControllerGetDevices**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetDevices) | **GET** /{tenant}/devices | Get all devices information
[**modulesDeviceHttpControllersDeviceControllerGetNrms**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerGetNrms) | **GET** /{tenant}/devices/{imei}/nrms | Get all NRM&#39;s for a specific device
[**modulesDeviceHttpControllersDeviceControllerUpdateDevice**](DevicesApi.md#modulesDeviceHttpControllersDeviceControllerUpdateDevice) | **PATCH** /{tenant}/devices/{imei} | Updates a specific device
[**modulesDeviceHttpControllersDevicePositionControllerGetDevicePosition**](DevicesApi.md#modulesDeviceHttpControllersDevicePositionControllerGetDevicePosition) | **GET** /{tenant}/devices/{imei}/position | Get the current position for a specific device
[**modulesDeviceHttpControllersDevicePositionControllerGetDevicePositions**](DevicesApi.md#modulesDeviceHttpControllersDevicePositionControllerGetDevicePositions) | **GET** /{tenant}/devices/{imei}/positions | Get positions collection for a specific device within given time-frame
[**modulesDeviceHttpControllersDoorControllerClose**](DevicesApi.md#modulesDeviceHttpControllersDoorControllerClose) | **PUT** /{tenant}/devices/{imei}/doorclose | Closes the doors of specific device
[**modulesDeviceHttpControllersDoorControllerOpen**](DevicesApi.md#modulesDeviceHttpControllersDoorControllerOpen) | **PUT** /{tenant}/devices/{imei}/dooropen | Open the doors of specific device



## modulesDeviceHttpControllersDeviceControllerCreateDevice

> InlineResponse200 modulesDeviceHttpControllersDeviceControllerCreateDevice(tenant, createDeviceAcceptableBodyRequest)

Creates a device

Adds a device into our database

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createDeviceAcceptableBodyRequest = new PlatformApi.CreateDeviceAcceptableBodyRequest(); // CreateDeviceAcceptableBodyRequest | Device object that needs to be added to the platform
apiInstance.modulesDeviceHttpControllersDeviceControllerCreateDevice(tenant, createDeviceAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createDeviceAcceptableBodyRequest** | [**CreateDeviceAcceptableBodyRequest**](CreateDeviceAcceptableBodyRequest.md)| Device object that needs to be added to the platform | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerDeleteDevice

> AnyType modulesDeviceHttpControllersDeviceControllerDeleteDevice(tenant, imei)

Deletes a device

Delete a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDeviceControllerDeleteDevice(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDevice

> DeviceDTO modulesDeviceHttpControllersDeviceControllerGetDevice(imei, tenant)

Get a single device information

Returns single device data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let imei = 56; // Number | Device IMEI
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDevice(imei, tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **imei** | **Number**| Device IMEI | 
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**DeviceDTO**](DeviceDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceAlerts

> [DeviceAlertDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceAlerts(tenant, imei, opts)

Get all alerts for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56 // Number | Timeframe end time in epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceAlerts(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 

### Return type

[**[DeviceAlertDTO]**](DeviceAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceGeoAlerts

> [GeoAlertDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceGeoAlerts(tenant, imei, opts)

Get all geo alerts for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56 // Number | Timeframe end time in epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceGeoAlerts(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 

### Return type

[**[GeoAlertDTO]**](GeoAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceIdleEvents

> [IdleEventDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceIdleEvents(tenant, imei, opts)

Get all idle events for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56 // Number | Timeframe end time in epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceIdleEvents(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 

### Return type

[**[IdleEventDTO]**](IdleEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceImpacts

> [ImpactDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceImpacts(tenant, imei, opts)

Get all impacts for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56 // Number | Timeframe end time in epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceImpacts(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 

### Return type

[**[ImpactDTO]**](ImpactDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceJourneys

> [JourneyDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceJourneys(tenant, imei, opts)

Get all journeys for a specific device

Returns journeys data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56, // Number | Timeframe end time in epm
  'epm': 56 // Number | Filters journeys so that returned journeys' start_timestamp is lower than provided epm and end_timestamp is bigger than provided epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceJourneys(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 
 **epm** | **Number**| Filters journeys so that returned journeys&#39; start_timestamp is lower than provided epm and end_timestamp is bigger than provided epm | [optional] 

### Return type

[**[JourneyDTO]**](JourneyDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceRentals

> [RentalDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceRentals(tenant, imei)

Get all rentals for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceRentals(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**[RentalDTO]**](RentalDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceRepairOrders

> RepairOrderModel modulesDeviceHttpControllersDeviceControllerGetDeviceRepairOrders(tenant, imei)

Get all repair orders for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceRepairOrders(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**RepairOrderModel**](RepairOrderModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDeviceSpeedEvents

> [SpeedEventDTO] modulesDeviceHttpControllersDeviceControllerGetDeviceSpeedEvents(tenant, imei, opts)

Get all Speed events for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let opts = {
  'start': 56, // Number | Timeframe start time in epm
  'end': 56 // Number | Timeframe end time in epm
};
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDeviceSpeedEvents(tenant, imei, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | [optional] 
 **end** | **Number**| Timeframe end time in epm | [optional] 

### Return type

[**[SpeedEventDTO]**](SpeedEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetDevices

> [DeviceDTO] modulesDeviceHttpControllersDeviceControllerGetDevices(tenant)

Get all devices information

Returns devices data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesDeviceHttpControllersDeviceControllerGetDevices(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**[DeviceDTO]**](DeviceDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerGetNrms

> [NRMModel] modulesDeviceHttpControllersDeviceControllerGetNrms(tenant, imei)

Get all NRM&#39;s for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDeviceControllerGetNrms(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**[NRMModel]**](NRMModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDeviceControllerUpdateDevice

> AnyType modulesDeviceHttpControllersDeviceControllerUpdateDevice(tenant, imei, updateDeviceAcceptableBodyRequest)

Updates a specific device

Updates whatever fields are been sent

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let updateDeviceAcceptableBodyRequest = new PlatformApi.UpdateDeviceAcceptableBodyRequest(); // UpdateDeviceAcceptableBodyRequest | Updates the property sent with the value
apiInstance.modulesDeviceHttpControllersDeviceControllerUpdateDevice(tenant, imei, updateDeviceAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **updateDeviceAcceptableBodyRequest** | [**UpdateDeviceAcceptableBodyRequest**](UpdateDeviceAcceptableBodyRequest.md)| Updates the property sent with the value | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesDeviceHttpControllersDevicePositionControllerGetDevicePosition

> PositionDTO modulesDeviceHttpControllersDevicePositionControllerGetDevicePosition(tenant, imei)

Get the current position for a specific device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDevicePositionControllerGetDevicePosition(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**PositionDTO**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDevicePositionControllerGetDevicePositions

> [PositionDTO] modulesDeviceHttpControllersDevicePositionControllerGetDevicePositions(tenant, imei, start, end)

Get positions collection for a specific device within given time-frame

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let start = 56; // Number | Timeframe start time in epm
let end = 56; // Number | Timeframe end time in epm
apiInstance.modulesDeviceHttpControllersDevicePositionControllerGetDevicePositions(tenant, imei, start, end, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **start** | **Number**| Timeframe start time in epm | 
 **end** | **Number**| Timeframe end time in epm | 

### Return type

[**[PositionDTO]**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDoorControllerClose

> InlineResponse2001 modulesDeviceHttpControllersDoorControllerClose(tenant, imei)

Closes the doors of specific device

Closes the doors of a vehicle on a compatible device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDoorControllerClose(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDeviceHttpControllersDoorControllerOpen

> InlineResponse2002 modulesDeviceHttpControllersDoorControllerOpen(tenant, imei)

Open the doors of specific device

Opens the doors of a vehicle on a compatible device

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DevicesApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
apiInstance.modulesDeviceHttpControllersDoorControllerOpen(tenant, imei, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

