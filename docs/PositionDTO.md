# PlatformApi.PositionDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Number** |  | [optional] 
**latitude** | **Number** |  | [optional] 
**longitude** | **Number** |  | [optional] 
**speed** | **Number** |  | [optional] 
**bearing** | **Number** |  | [optional] 
**altitude** | **Number** |  | [optional] 
**fixValid** | **Boolean** |  | [optional] 
**timeValid** | **Boolean** |  | [optional] 
**accuracy** | **String** |  | [optional] 


