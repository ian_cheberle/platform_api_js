# PlatformApi.RepairOrdersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRepairOrderHttpControllersRepairOrderControllerCreateRepairOrder**](RepairOrdersApi.md#modulesRepairOrderHttpControllersRepairOrderControllerCreateRepairOrder) | **POST** /{tenant}/repairorders | Creates a single Repair order
[**modulesRepairOrderHttpControllersRepairOrderControllerDeleteRepairOrder**](RepairOrdersApi.md#modulesRepairOrderHttpControllersRepairOrderControllerDeleteRepairOrder) | **DELETE** /{tenant}/repairorders/{repair_number} | Deletes a single Repair order
[**modulesRepairOrderHttpControllersRepairOrderControllerGetAllRepairOrders**](RepairOrdersApi.md#modulesRepairOrderHttpControllersRepairOrderControllerGetAllRepairOrders) | **GET** /{tenant}/repairorders | Get all RepairOrders
[**modulesRepairOrderHttpControllersRepairOrderControllerGetRepairOrder**](RepairOrdersApi.md#modulesRepairOrderHttpControllersRepairOrderControllerGetRepairOrder) | **GET** /{tenant}/repairorders/{repair_number} | Gets an Repair order based on the repair_number
[**modulesRepairOrderHttpControllersRepairOrderControllerUpdateRepairOrder**](RepairOrdersApi.md#modulesRepairOrderHttpControllersRepairOrderControllerUpdateRepairOrder) | **PATCH** /{tenant}/repairorders/{repair_number} | Update an specific Repair order



## modulesRepairOrderHttpControllersRepairOrderControllerCreateRepairOrder

> InlineResponse2014 modulesRepairOrderHttpControllersRepairOrderControllerCreateRepairOrder(tenant, createRepairOrderAcceptableBodyRequest)

Creates a single Repair order

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RepairOrdersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createRepairOrderAcceptableBodyRequest = new PlatformApi.CreateRepairOrderAcceptableBodyRequest(); // CreateRepairOrderAcceptableBodyRequest | RepairOrder object that needs to be added to the platform
apiInstance.modulesRepairOrderHttpControllersRepairOrderControllerCreateRepairOrder(tenant, createRepairOrderAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createRepairOrderAcceptableBodyRequest** | [**CreateRepairOrderAcceptableBodyRequest**](CreateRepairOrderAcceptableBodyRequest.md)| RepairOrder object that needs to be added to the platform | 

### Return type

[**InlineResponse2014**](InlineResponse2014.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRepairOrderHttpControllersRepairOrderControllerDeleteRepairOrder

> modulesRepairOrderHttpControllersRepairOrderControllerDeleteRepairOrder(tenant, repairNumber)

Deletes a single Repair order

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RepairOrdersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let repairNumber = "repairNumber_example"; // String | RepairOrder number
apiInstance.modulesRepairOrderHttpControllersRepairOrderControllerDeleteRepairOrder(tenant, repairNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **repairNumber** | **String**| RepairOrder number | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesRepairOrderHttpControllersRepairOrderControllerGetAllRepairOrders

> AllRepairOrdersPaginated modulesRepairOrderHttpControllersRepairOrderControllerGetAllRepairOrders(tenant, opts)

Get all RepairOrders

Returns repair orders data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RepairOrdersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'rangeType': ["null"], // [String] | 
  'start': "start_example", // String | 
  'end': "end_example", // String | 
  'vid': "vid_example", // String | 
  'driverId': "driverId_example" // String | 
};
apiInstance.modulesRepairOrderHttpControllersRepairOrderControllerGetAllRepairOrders(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **rangeType** | [**[String]**](String.md)|  | [optional] 
 **start** | **String**|  | [optional] 
 **end** | **String**|  | [optional] 
 **vid** | **String**|  | [optional] 
 **driverId** | **String**|  | [optional] 

### Return type

[**AllRepairOrdersPaginated**](AllRepairOrdersPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRepairOrderHttpControllersRepairOrderControllerGetRepairOrder

> RepairOrderModel modulesRepairOrderHttpControllersRepairOrderControllerGetRepairOrder(tenant, repairNumber)

Gets an Repair order based on the repair_number

Returns a repairOrder

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RepairOrdersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let repairNumber = "repairNumber_example"; // String | 
apiInstance.modulesRepairOrderHttpControllersRepairOrderControllerGetRepairOrder(tenant, repairNumber, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **repairNumber** | **String**|  | 

### Return type

[**RepairOrderModel**](RepairOrderModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRepairOrderHttpControllersRepairOrderControllerUpdateRepairOrder

> modulesRepairOrderHttpControllersRepairOrderControllerUpdateRepairOrder(tenant, repairNumber, updateRepairOrderAcceptableBodyRequest)

Update an specific Repair order

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RepairOrdersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let repairNumber = "repairNumber_example"; // String | RepairOrder number
let updateRepairOrderAcceptableBodyRequest = new PlatformApi.UpdateRepairOrderAcceptableBodyRequest(); // UpdateRepairOrderAcceptableBodyRequest | RepairOrder values to be updated
apiInstance.modulesRepairOrderHttpControllersRepairOrderControllerUpdateRepairOrder(tenant, repairNumber, updateRepairOrderAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **repairNumber** | **String**| RepairOrder number | 
 **updateRepairOrderAcceptableBodyRequest** | [**UpdateRepairOrderAcceptableBodyRequest**](UpdateRepairOrderAcceptableBodyRequest.md)| RepairOrder values to be updated | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

