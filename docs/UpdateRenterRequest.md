# PlatformApi.UpdateRenterRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**emailVerifiedAt** | **String** |  | [optional] 
**accountType** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**dob** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**notificationToken** | **String** |  | [optional] 


