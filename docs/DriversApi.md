# PlatformApi.DriversApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesDriverHttpControllersDriverControllerCreateDriver**](DriversApi.md#modulesDriverHttpControllersDriverControllerCreateDriver) | **POST** /{tenant}/drivers | Creates a single Driver
[**modulesDriverHttpControllersDriverControllerDeleteDriver**](DriversApi.md#modulesDriverHttpControllersDriverControllerDeleteDriver) | **DELETE** /{tenant}/drivers/{driver_id} | Deletes a single Driver
[**modulesDriverHttpControllersDriverControllerGetDeviceAlerts**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDeviceAlerts) | **GET** /{tenant}/drivers/{driver_id}/devicealerts | Get all device alerts based on the driver id
[**modulesDriverHttpControllersDriverControllerGetDriver**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriver) | **GET** /{tenant}/drivers/{driver_id} | Finds a single driver
[**modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts) | **GET** /{tenant}/drivers/{driver_id}/geoalerts | Get all geo alerts based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverIdleEvents**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverIdleEvents) | **GET** /{tenant}/drivers/{driver_id}/idleevents | Get all idle events based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverImpacts**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverImpacts) | **GET** /{tenant}/drivers/{driver_id}/impacts | Get all impacts based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverJourneys**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverJourneys) | **GET** /{tenant}/drivers/{driver_id}/journeys | Get all journeys based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverPosition**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverPosition) | **GET** /{tenant}/drivers/{driver_id}/position | Get latest position for a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverRentals**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverRentals) | **GET** /{tenant}/drivers/{driver_id}/rentals | Get all rentals based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents) | **GET** /{tenant}/drivers/{driver_id}/speedevents | Get all speed events based on a specific driver
[**modulesDriverHttpControllersDriverControllerGetDrivers**](DriversApi.md#modulesDriverHttpControllersDriverControllerGetDrivers) | **GET** /{tenant}/drivers | Get all Drivers
[**modulesDriverHttpControllersDriverControllerUpdateDriver**](DriversApi.md#modulesDriverHttpControllersDriverControllerUpdateDriver) | **PATCH** /{tenant}/drivers/{driver_id} | Update an specific Driver



## modulesDriverHttpControllersDriverControllerCreateDriver

> InlineResponse2011 modulesDriverHttpControllersDriverControllerCreateDriver(tenant, createDriverAcceptableBodyRequest)

Creates a single Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createDriverAcceptableBodyRequest = new PlatformApi.CreateDriverAcceptableBodyRequest(); // CreateDriverAcceptableBodyRequest | Driver object that needs to be added to the platform
apiInstance.modulesDriverHttpControllersDriverControllerCreateDriver(tenant, createDriverAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createDriverAcceptableBodyRequest** | [**CreateDriverAcceptableBodyRequest**](CreateDriverAcceptableBodyRequest.md)| Driver object that needs to be added to the platform | 

### Return type

[**InlineResponse2011**](InlineResponse2011.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerDeleteDriver

> modulesDriverHttpControllersDriverControllerDeleteDriver(tenant, driverId)

Deletes a single Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
apiInstance.modulesDriverHttpControllersDriverControllerDeleteDriver(tenant, driverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## modulesDriverHttpControllersDriverControllerGetDeviceAlerts

> [DeviceAlertDTO] modulesDriverHttpControllersDriverControllerGetDeviceAlerts(tenant, driverId, opts)

Get all device alerts based on the driver id

Returns device alerts data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDeviceAlerts(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[DeviceAlertDTO]**](DeviceAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriver

> DriverModel modulesDriverHttpControllersDriverControllerGetDriver(tenant, driverId)

Finds a single driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
apiInstance.modulesDriverHttpControllersDriverControllerGetDriver(tenant, driverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 

### Return type

[**DriverModel**](DriverModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts

> [GeoAlertDTO] modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts(tenant, driverId, opts)

Get all geo alerts based on a specific driver

Returns a driver geo alerts data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Driver)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Driver) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[GeoAlertDTO]**](GeoAlertDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverIdleEvents

> [IdleEventDTO] modulesDriverHttpControllersDriverControllerGetDriverIdleEvents(tenant, driverId, opts)

Get all idle events based on a specific driver

Get all device idle events of a Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Driver)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverIdleEvents(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Driver) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[IdleEventDTO]**](IdleEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverImpacts

> [ImpactDTO] modulesDriverHttpControllersDriverControllerGetDriverImpacts(tenant, driverId, opts)

Get all impacts based on a specific driver

Returns a driver impacts data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverImpacts(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[ImpactDTO]**](ImpactDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverJourneys

> [JourneyDTO] modulesDriverHttpControllersDriverControllerGetDriverJourneys(tenant, driverId, opts)

Get all journeys based on a specific driver

Get all journeys of a Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverJourneys(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[JourneyDTO]**](JourneyDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverPosition

> [PositionDTO] modulesDriverHttpControllersDriverControllerGetDriverPosition(tenant, driverId)

Get latest position for a specific driver

Return latest position for the driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverPosition(tenant, driverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 

### Return type

[**[PositionDTO]**](PositionDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverRentals

> [RentalModel] modulesDriverHttpControllersDriverControllerGetDriverRentals(tenant, driverId, opts)

Get all rentals based on a specific driver

Get all rentals of a Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverRentals(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[RentalModel]**](RentalModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents

> [SpeedEventDTO] modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents(tenant, driverId, opts)

Get all speed events based on a specific driver

Get all device speed events of a Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Driver)
let driverId = "driverId_example"; // String | Driver Id
let opts = {
  'limit': null // Number | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents(tenant, driverId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Driver) | 
 **driverId** | **String**| Driver Id | 
 **limit** | [**Number**](.md)|  | [optional] 

### Return type

[**[SpeedEventDTO]**](SpeedEventDTO.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerGetDrivers

> AllDriversPaginated modulesDriverHttpControllersDriverControllerGetDrivers(tenant, opts)

Get all Drivers

Returns driver data paginated

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'groupId': "groupId_example" // String | 
};
apiInstance.modulesDriverHttpControllersDriverControllerGetDrivers(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **groupId** | **String**|  | [optional] 

### Return type

[**AllDriversPaginated**](AllDriversPaginated.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesDriverHttpControllersDriverControllerUpdateDriver

> AnyType modulesDriverHttpControllersDriverControllerUpdateDriver(tenant, driverId, updateDriverAcceptableBodyRequest)

Update an specific Driver

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DriversApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let driverId = "driverId_example"; // String | Driver Id
let updateDriverAcceptableBodyRequest = new PlatformApi.UpdateDriverAcceptableBodyRequest(); // UpdateDriverAcceptableBodyRequest | Driver values to be updated
apiInstance.modulesDriverHttpControllersDriverControllerUpdateDriver(tenant, driverId, updateDriverAcceptableBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **driverId** | **String**| Driver Id | 
 **updateDriverAcceptableBodyRequest** | [**UpdateDriverAcceptableBodyRequest**](UpdateDriverAcceptableBodyRequest.md)| Driver values to be updated | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

