# PlatformApi.CreateDriverAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**dob** | **Date** |  | [optional] 
**email** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**nationality** | **String** |  | [optional] 
**occupation** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 


