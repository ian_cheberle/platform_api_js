# PlatformApi.CreateStationAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **String** |  | [optional] 
**displayName** | **String** |  | [optional] 


