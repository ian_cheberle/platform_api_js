# PlatformApi.RentalExtraModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**extrasDesc** | **String** |  | [optional] 


