# PlatformApi.RepairOrderModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endTimestamp** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**repairNumber** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsOdometerIn** | **Number** |  | [optional] 
**rsOdometerOut** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**summary** | **String** |  | [optional] 
**imei** | **Number** |  | [optional] 
**reasonName** | **String** |  | [optional] 


