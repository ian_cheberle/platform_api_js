# PlatformApi.DeviceDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imei** | **Number** |  | [optional] 
**msisdn** | **Number** |  | [optional] 
**iccid** | **Number** |  | [optional] 
**manufacturer** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**network** | **String** |  | [optional] 
**purchasedDate** | **Number** |  | [optional] 
**mxp** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**position** | [**AnyType**](.md) |  | [optional] 


