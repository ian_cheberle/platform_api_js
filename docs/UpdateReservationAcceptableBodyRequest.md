# PlatformApi.UpdateReservationAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**epmEnd** | **String** |  | [optional] 
**epmStart** | **String** |  | [optional] 
**reservationId** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**status** | **String** |  | [optional] 


