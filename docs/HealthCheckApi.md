# PlatformApi.HealthCheckApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesHealthCheckHttpControllersHealthCheckControllerHealthCheck**](HealthCheckApi.md#modulesHealthCheckHttpControllersHealthCheckControllerHealthCheck) | **GET** /{tenant}/healthcheck | Preform database healthcheck



## modulesHealthCheckHttpControllersHealthCheckControllerHealthCheck

> AnyType modulesHealthCheckHttpControllersHealthCheckControllerHealthCheck(tenant)

Preform database healthcheck

Returns status code

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.HealthCheckApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesHealthCheckHttpControllersHealthCheckControllerHealthCheck(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

