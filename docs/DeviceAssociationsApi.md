# PlatformApi.DeviceAssociationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerPair**](DeviceAssociationsApi.md#modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerPair) | **POST** /{tenant}/devices/{imei}/pair | Pair a device and a vehicle
[**modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerUnpair**](DeviceAssociationsApi.md#modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerUnpair) | **PATCH** /{tenant}/devices/{imei}/unpair | Unpair a device and a vehicle



## modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerPair

> AnyType modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerPair(tenant, imei, pairDeviceVehicleBodyRequest)

Pair a device and a vehicle

Returns status code with an empty body

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DeviceAssociationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let pairDeviceVehicleBodyRequest = new PlatformApi.PairDeviceVehicleBodyRequest(); // PairDeviceVehicleBodyRequest | Pair request body containing registration and other meta data
apiInstance.modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerPair(tenant, imei, pairDeviceVehicleBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **pairDeviceVehicleBodyRequest** | [**PairDeviceVehicleBodyRequest**](PairDeviceVehicleBodyRequest.md)| Pair request body containing registration and other meta data | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerUnpair

> AnyType modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerUnpair(tenant, imei, unpairDeviceVehicleBodyRequest)

Unpair a device and a vehicle

Returns status code with an empty body

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.DeviceAssociationsApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let imei = 56; // Number | Device IMEI
let unpairDeviceVehicleBodyRequest = new PlatformApi.UnpairDeviceVehicleBodyRequest(); // UnpairDeviceVehicleBodyRequest | Unpair request body containing registration and other meta data
apiInstance.modulesDeviceVehiclePairHttpControllersDeviceVehiclePairControllerUnpair(tenant, imei, unpairDeviceVehicleBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **imei** | **Number**| Device IMEI | 
 **unpairDeviceVehicleBodyRequest** | [**UnpairDeviceVehicleBodyRequest**](UnpairDeviceVehicleBodyRequest.md)| Unpair request body containing registration and other meta data | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

