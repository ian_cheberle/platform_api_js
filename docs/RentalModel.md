# PlatformApi.RentalModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cdw** | **Number** |  | [optional] 
**collectionId** | **Number** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**customerName** | **String** |  | [optional] 
**cancelledTimestamp** | **String** |  | [optional] 
**closedTimestamp** | **String** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**delCol** | **Number** |  | [optional] 
**deliveryId** | **Number** |  | [optional] 
**driverId** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**longTerm** | **Number** |  | [optional] 
**origin** | **String** |  | [optional] 
**postpayFuel** | **Boolean** |  | [optional] 
**prepayFuel** | **Boolean** |  | [optional] 
**previousRa** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**rateCode** | **String** |  | [optional] 
**rentalStatus** | **String** |  | [optional] 
**scdw** | **String** |  | [optional] 
**selfInsuredRa** | **Boolean** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**salesRep** | **Boolean** |  | [optional] 


