# PlatformApi.InlineResponse2004

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokenType** | **String** |  | [optional] 
**expiresIn** | **Number** |  | [optional] 
**accessToken** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 


