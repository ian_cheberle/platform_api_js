# PlatformApi.RenterApiAuthenticationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRenterUserHttpControllersAuthAuthControllerLogin**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerLogin) | **POST** /{tenant}/counterless/login/local | Login into RenterApi in order to obtain OAuth token
[**modulesRenterUserHttpControllersAuthAuthControllerLogout**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerLogout) | **GET** /{tenant}/counterless/logout | Logs the Renter User out 
[**modulesRenterUserHttpControllersAuthAuthControllerRefresh**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerRefresh) | **POST** /{tenant}/counterless/refresh | Refresh access token in order to obtain a new OAuth token
[**modulesRenterUserHttpControllersAuthAuthControllerRegister**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerRegister) | **POST** /{tenant}/counterless/register | Registers a new RenterApi User (RenterUser)
[**modulesRenterUserHttpControllersAuthAuthControllerUser**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerUser) | **GET** /{tenant}/counterless/auth/user | Returns current user data
[**modulesRenterUserHttpControllersAuthAuthControllerVerifyEmail**](RenterApiAuthenticationApi.md#modulesRenterUserHttpControllersAuthAuthControllerVerifyEmail) | **PATCH** /{tenant}/counterless/verify | Verifies RenterApi User (RenterUser) Email



## modulesRenterUserHttpControllersAuthAuthControllerLogin

> InlineResponse2004 modulesRenterUserHttpControllersAuthAuthControllerLogin(tenant, inlineObject1)

Login into RenterApi in order to obtain OAuth token

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let inlineObject1 = new PlatformApi.InlineObject1(); // InlineObject1 | 
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerLogin(tenant, inlineObject1, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **inlineObject1** | [**InlineObject1**](InlineObject1.md)|  | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRenterUserHttpControllersAuthAuthControllerLogout

> InlineResponse2005 modulesRenterUserHttpControllersAuthAuthControllerLogout(tenant)

Logs the Renter User out 

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerLogout(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersAuthAuthControllerRefresh

> InlineResponse2004 modulesRenterUserHttpControllersAuthAuthControllerRefresh(tenant, inlineObject2)

Refresh access token in order to obtain a new OAuth token

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let inlineObject2 = new PlatformApi.InlineObject2(); // InlineObject2 | 
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerRefresh(tenant, inlineObject2, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **inlineObject2** | [**InlineObject2**](InlineObject2.md)|  | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRenterUserHttpControllersAuthAuthControllerRegister

> AnyType modulesRenterUserHttpControllersAuthAuthControllerRegister(tenant, inlineObject)

Registers a new RenterApi User (RenterUser)

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let inlineObject = new PlatformApi.InlineObject(); // InlineObject | 
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerRegister(tenant, inlineObject, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **inlineObject** | [**InlineObject**](InlineObject.md)|  | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## modulesRenterUserHttpControllersAuthAuthControllerUser

> RenterUserModel modulesRenterUserHttpControllersAuthAuthControllerUser(tenant)

Returns current user data

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerUser(tenant, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 

### Return type

[**RenterUserModel**](RenterUserModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersAuthAuthControllerVerifyEmail

> AnyType modulesRenterUserHttpControllersAuthAuthControllerVerifyEmail(tenant, verifyBodyRequest)

Verifies RenterApi User (RenterUser) Email

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RenterApiAuthenticationApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let verifyBodyRequest = new PlatformApi.VerifyBodyRequest(); // VerifyBodyRequest | Verfication data. The Verification Token is obtained via Registration Email
apiInstance.modulesRenterUserHttpControllersAuthAuthControllerVerifyEmail(tenant, verifyBodyRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **verifyBodyRequest** | [**VerifyBodyRequest**](VerifyBodyRequest.md)| Verfication data. The Verification Token is obtained via Registration Email | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

