# PlatformApi.CreateRentalAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cdw** | **Number** |  | [optional] 
**colId** | **Number** |  | [optional] 
**currentLocation** | **String** |  | [optional] 
**customerName** | **String** |  | [optional] 
**dateCancelled** | **String** |  | [optional] 
**dateClosed** | **String** |  | [optional] 
**epmStart** | **String** |  | [optional] 
**epmEnd** | **String** |  | [optional] 
**delCol** | **Number** |  | [optional] 
**deliveryId** | **Number** |  | [optional] 
**driverId** | **String** |  | [optional] 
**dueBackLocation** | **String** |  | [optional] 
**longTerm** | **Number** |  | [optional] 
**origin** | **String** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**postpayFuel** | **Boolean** |  | [optional] 
**prepayFuel** | **Boolean** |  | [optional] 
**previousRa** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**rateCode** | **String** |  | [optional] 
**rentalStatus** | **String** |  | [optional] 
**salesRep** | **String** |  | [optional] 
**scdw** | **String** |  | [optional] 
**selfInsuredRa** | **Boolean** |  | [optional] 


