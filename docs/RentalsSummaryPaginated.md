# PlatformApi.RentalsSummaryPaginated

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPage** | **Number** |  | [optional] 
**firstPageUrl** | **String** |  | [optional] 
**from** | **Number** |  | [optional] 
**lastPage** | **Number** |  | [optional] 
**lastPageUrl** | **String** |  | [optional] 
**nextPageUrl** | **String** |  | [optional] 
**path** | **String** |  | [optional] 
**perPage** | **Number** |  | [optional] 
**prevPageUrl** | **String** |  | [optional] 
**to** | **Number** |  | [optional] 
**total** | **Number** |  | [optional] 
**data** | [**[RentalSummaryModel]**](RentalSummaryModel.md) |  | [optional] 


