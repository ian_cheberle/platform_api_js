# PlatformApi.LicenceModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**driverId** | **String** |  | [optional] 
**licenceExpiry** | **Number** |  | [optional] 
**licenceIssue** | **Number** |  | [optional] 
**licenceApproved** | **Boolean** |  | [optional] 
**licenceApprovedUser** | **String** |  | [optional] 
**licenceApprovedTime** | **Number** |  | [optional] 
**licenceRejected** | **Boolean** |  | [optional] 
**licenceRejectedUser** | **String** |  | [optional] 
**licenceRejectedReason** | **String** |  | [optional] 
**licenceRejectedTime** | **Number** |  | [optional] 
**activeLicence** | **Boolean** |  | [optional] 


