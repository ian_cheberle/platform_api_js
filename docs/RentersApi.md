# PlatformApi.RentersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**modulesRenterUserHttpControllersRenterRenterControllerCreateRenter**](RentersApi.md#modulesRenterUserHttpControllersRenterRenterControllerCreateRenter) | **POST** /{tenant}/counterless/renters | Creates a new RenterApi User (RenterUser)
[**modulesRenterUserHttpControllersRenterRenterControllerDeleteRenter**](RentersApi.md#modulesRenterUserHttpControllersRenterRenterControllerDeleteRenter) | **DELETE** /{tenant}/counterless/renters/{renterId} | Deletes a Renter
[**modulesRenterUserHttpControllersRenterRenterControllerGetRenter**](RentersApi.md#modulesRenterUserHttpControllersRenterRenterControllerGetRenter) | **GET** /{tenant}/counterless/renters/{renterId} | Finds a single renter
[**modulesRenterUserHttpControllersRenterRenterControllerGetRenters**](RentersApi.md#modulesRenterUserHttpControllersRenterRenterControllerGetRenters) | **GET** /{tenant}/counterless/renters | Returns paginated renter users collection
[**modulesRenterUserHttpControllersRenterRenterControllerUpdateRenter**](RentersApi.md#modulesRenterUserHttpControllersRenterRenterControllerUpdateRenter) | **PATCH** /{tenant}/counterless/renters/{renterId} | Updates RenterApi User (RenterUser)



## modulesRenterUserHttpControllersRenterRenterControllerCreateRenter

> modulesRenterUserHttpControllersRenterRenterControllerCreateRenter(tenant, createRenterRequest)

Creates a new RenterApi User (RenterUser)

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let createRenterRequest = new PlatformApi.CreateRenterRequest(); // CreateRenterRequest | User data
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerCreateRenter(tenant, createRenterRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **createRenterRequest** | [**CreateRenterRequest**](CreateRenterRequest.md)| User data | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## modulesRenterUserHttpControllersRenterRenterControllerDeleteRenter

> AnyType modulesRenterUserHttpControllersRenterRenterControllerDeleteRenter(tenant, renterId)

Deletes a Renter

Delete a specific renter

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter of the request (Driver Id)
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerDeleteRenter(tenant, renterId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter of the request (Driver Id) | 

### Return type

[**AnyType**](AnyType.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersRenterRenterControllerGetRenter

> RenterUserModel modulesRenterUserHttpControllersRenterRenterControllerGetRenter(tenant, renterId)

Finds a single renter

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter Id
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerGetRenter(tenant, renterId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter Id | 

### Return type

[**RenterUserModel**](RenterUserModel.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersRenterRenterControllerGetRenters

> InlineResponse2006 modulesRenterUserHttpControllersRenterRenterControllerGetRenters(tenant, opts)

Returns paginated renter users collection

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let opts = {
  'perPage': 56, // Number | Number of items per page
  'page': 56 // Number | Current page
};
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerGetRenters(tenant, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **perPage** | **Number**| Number of items per page | [optional] 
 **page** | **Number**| Current page | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## modulesRenterUserHttpControllersRenterRenterControllerUpdateRenter

> modulesRenterUserHttpControllersRenterRenterControllerUpdateRenter(tenant, renterId, updateRenterRequest)

Updates RenterApi User (RenterUser)

### Example

```javascript
import PlatformApi from 'platform_api';
let defaultClient = PlatformApi.ApiClient.instance;
// Configure Bearer (JWT) access token for authorization: bearerAuth
let bearerAuth = defaultClient.authentications['bearerAuth'];
bearerAuth.accessToken = "YOUR ACCESS TOKEN"

let apiInstance = new PlatformApi.RentersApi();
let tenant = "tenant_example"; // String | Origin of the request (Company)
let renterId = "renterId_example"; // String | Renter whose data should be updated
let updateRenterRequest = new PlatformApi.UpdateRenterRequest(); // UpdateRenterRequest | User data
apiInstance.modulesRenterUserHttpControllersRenterRenterControllerUpdateRenter(tenant, renterId, updateRenterRequest, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenant** | **String**| Origin of the request (Company) | 
 **renterId** | **String**| Renter whose data should be updated | 
 **updateRenterRequest** | [**UpdateRenterRequest**](UpdateRenterRequest.md)| User data | 

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

