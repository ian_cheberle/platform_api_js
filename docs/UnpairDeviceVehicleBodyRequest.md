# PlatformApi.UnpairDeviceVehicleBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**phoneLat** | **Number** |  | [optional] 
**phoneLon** | **Number** |  | [optional] 
**registration** | **String** |  | [optional] 


