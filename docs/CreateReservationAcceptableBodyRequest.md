# PlatformApi.CreateReservationAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **String** |  | [optional] 
**endTimestamp** | **String** |  | [optional] 
**startTimestamp** | **String** |  | [optional] 
**reservationId** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**status** | **String** |  | [optional] 


