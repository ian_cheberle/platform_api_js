# PlatformApi.DriverModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**dob** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**groupId** | **Number** |  | [optional] 
**nationality** | **String** |  | [optional] 
**occupation** | **String** |  | [optional] 
**safetyCompetition** | **Boolean** |  | [optional] 
**emailCustomerUpdates** | **Boolean** |  | [optional] 


