# PlatformApi.JourneyModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imei** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**endTimestamp** | **String** |  | [optional] 
**distance** | **String** |  | [optional] 
**duration** | **Number** |  | [optional] 
**averageSpeed** | **String** |  | [optional] 
**idleDuration** | **Number** |  | [optional] 
**scoreFatigue** | **String** |  | [optional] 
**scoreIdle** | **String** |  | [optional] 
**scoreOverall** | **String** |  | [optional] 
**scoreSpeed** | **String** |  | [optional] 
**scoreSmooth** | **String** |  | [optional] 
**scoreTimeofday** | **String** |  | [optional] 


