# PlatformApi.CreateRentalAssociationAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimestamp** | **String** |  | [optional] 
**endTimestamp** | **String** |  | [optional] 
**externalId** | **Number** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsOdometerIn** | **Number** |  | [optional] 
**rsOdometerOut** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 


