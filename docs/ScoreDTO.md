# PlatformApi.ScoreDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**journeyId** | **Number** |  | [optional] 
**overall** | **Number** |  | [optional] 
**speed** | **Number** |  | [optional] 
**smooth** | **Number** |  | [optional] 
**timeOfDay** | **Number** |  | [optional] 
**idle** | **Number** |  | [optional] 
**fatigue** | **Number** |  | [optional] 


