# PlatformApi.InlineObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**acceptedTerms** | **Boolean** |  | [optional] 


