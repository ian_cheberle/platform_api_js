# PlatformApi.CreateCounterlessRentalAcceptableBodyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**raNumber** | **String** |  | [optional] 
**driverId** | **String** |  | [optional] 
**counterlessStatus** | **String** |  | [optional] 
**checkinAvailableFrom** | **Number** |  | [optional] 
**checkinClosedAfter** | **Number** |  | [optional] 
**checkinAvailableUserNotified** | **Boolean** |  | [optional] 
**checkedIn** | **Boolean** |  | [optional] 
**checkedInTime** | **Number** |  | [optional] 
**vehicleAssigned** | **Boolean** |  | [optional] 
**vehicleAssignedRegistration** | **String** |  | [optional] 
**atVehicle** | **Boolean** |  | [optional] 
**atVehicleTime** | **Number** |  | [optional] 
**keySwapActioned** | **Boolean** |  | [optional] 



## Enum: CounterlessStatusEnum


* `BOOKED` (value: `"BOOKED"`)

* `CHECKEDIN` (value: `"CHECKEDIN"`)

* `ARRIVED` (value: `"ARRIVED"`)

* `ACTIVE` (value: `"ACTIVE"`)

* `OVERDUE` (value: `"OVERDUE"`)

* `CANCELLED` (value: `"CANCELLED"`)




