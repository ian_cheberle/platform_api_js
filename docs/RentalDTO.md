# PlatformApi.RentalDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cdw** | **Number** |  | [optional] 
**collectionId** | **Number** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**customerName** | **String** |  | [optional] 
**cancelledTimestamp** | **Number** |  | [optional] 
**closedTimestamp** | **Number** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**delCol** | **Number** |  | [optional] 
**deliveryId** | **Number** |  | [optional] 
**driverId** | **Number** |  | [optional] 
**stationIn** | **String** |  | [optional] 
**longTerm** | **Number** |  | [optional] 
**origin** | **String** |  | [optional] 
**poNumber** | **String** |  | [optional] 
**postpayFuel** | **Number** |  | [optional] 
**prepayFuel** | **Number** |  | [optional] 
**previousRa** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**rateCode** | **String** |  | [optional] 
**rentalStatus** | **String** |  | [optional] 
**salesRep** | **String** |  | [optional] 
**scdw** | **Number** |  | [optional] 
**selfInsuredRa** | **Number** |  | [optional] 


