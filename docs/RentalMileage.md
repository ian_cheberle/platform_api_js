# PlatformApi.RentalMileage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registration** | **String** |  | [optional] 
**estRentalKms** | **Number** |  | [optional] 
**runningTotal** | **Number** |  | [optional] 
**rsKmsOut** | **Number** |  | [optional] 
**rsKmsIn** | **Number** |  | [optional] 
**dateIn** | **Date** |  | [optional] 
**dateOut** | **Date** |  | [optional] 
**vid** | **Number** |  | [optional] 


