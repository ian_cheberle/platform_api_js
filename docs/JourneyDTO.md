# PlatformApi.JourneyDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**imei** | **Number** |  | [optional] 
**startTimestamp** | [**AnyType**](.md) |  | [optional] 
**endTimestamp** | [**AnyType**](.md) |  | [optional] 
**distance** | **Number** |  | [optional] 
**duration** | **Number** |  | [optional] 
**averageSpeed** | **Number** |  | [optional] 
**idleDuration** | **Number** |  | [optional] 
**scoreFatigue** | **Number** |  | [optional] 
**scoreIdle** | **Number** |  | [optional] 
**scoreOverall** | **Number** | overall_score is called safety score too. Overall (safety) score is made up as a combination of the five features: smooth, speed, idle, fatigue and time of day so we can return these too | [optional] 
**scoreSpeed** | **Number** |  | [optional] 
**scoreSmooth** | **Number** |  | [optional] 
**scoreTimeofday** | **Number** |  | [optional] 
**driverId** | **String** |  | [optional] 
**raNumber** | **String** |  | [optional] 
**nrmNumber** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 


