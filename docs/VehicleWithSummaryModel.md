# PlatformApi.VehicleWithSummaryModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vid** | **Number** |  | [optional] 
**unitnr** | **Number** |  | [optional] 
**registration** | **String** |  | [optional] 
**make** | **String** |  | [optional] 
**model** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**colour** | **String** |  | [optional] 
**category** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**epm** | **Number** |  | [optional] 
**dateServiceDue** | **String** |  | [optional] 
**dueOffKms** | **String** |  | [optional] 
**customDueOffEpm** | **Number** |  | [optional] 
**customDueOffUser** | **String** |  | [optional] 
**createdBy** | **String** |  | [optional] 
**epmDueOff** | **Number** |  | [optional] 
**fleetType** | **String** |  | [optional] 
**lat** | **Number** |  | [optional] 
**lon** | **Number** |  | [optional] 
**imei** | **Number** |  | [optional] 


