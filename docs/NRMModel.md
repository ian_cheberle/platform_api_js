# PlatformApi.NRMModel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **String** |  | [optional] 
**endTimestamp** | **Number** |  | [optional] 
**startTimestamp** | **Number** |  | [optional] 
**nrmNumber** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**registration** | **String** |  | [optional] 
**rsOdometerIn** | **Number** |  | [optional] 
**rsOdometerOut** | **Number** |  | [optional] 
**vid** | **String** |  | [optional] 
**stationOut** | **String** |  | [optional] 
**stationIn** | **String** |  | [optional] 


