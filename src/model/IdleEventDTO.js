/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The IdleEventDTO model module.
 * @module model/IdleEventDTO
 * @version 1.0.0
 */
class IdleEventDTO {
    /**
     * Constructs a new <code>IdleEventDTO</code>.
     * @alias module:model/IdleEventDTO
     */
    constructor() { 
        
        IdleEventDTO.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>IdleEventDTO</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/IdleEventDTO} obj Optional instance to populate.
     * @return {module:model/IdleEventDTO} The populated <code>IdleEventDTO</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new IdleEventDTO();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('end_timestamp')) {
                obj['end_timestamp'] = ApiClient.convertToType(data['end_timestamp'], 'Number');
            }
            if (data.hasOwnProperty('start_timestamp')) {
                obj['start_timestamp'] = ApiClient.convertToType(data['start_timestamp'], 'Number');
            }
            if (data.hasOwnProperty('duration')) {
                obj['duration'] = ApiClient.convertToType(data['duration'], 'Number');
            }
            if (data.hasOwnProperty('latitude')) {
                obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
            }
            if (data.hasOwnProperty('longitude')) {
                obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
            }
            if (data.hasOwnProperty('imei')) {
                obj['imei'] = ApiClient.convertToType(data['imei'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} id
 */
IdleEventDTO.prototype['id'] = undefined;

/**
 * @member {Number} end_timestamp
 */
IdleEventDTO.prototype['end_timestamp'] = undefined;

/**
 * @member {Number} start_timestamp
 */
IdleEventDTO.prototype['start_timestamp'] = undefined;

/**
 * @member {Number} duration
 */
IdleEventDTO.prototype['duration'] = undefined;

/**
 * @member {Number} latitude
 */
IdleEventDTO.prototype['latitude'] = undefined;

/**
 * @member {Number} longitude
 */
IdleEventDTO.prototype['longitude'] = undefined;

/**
 * @member {Number} imei
 */
IdleEventDTO.prototype['imei'] = undefined;






export default IdleEventDTO;

