/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The VehicleWithSummaryModel model module.
 * @module model/VehicleWithSummaryModel
 * @version 1.0.0
 */
class VehicleWithSummaryModel {
    /**
     * Constructs a new <code>VehicleWithSummaryModel</code>.
     * @alias module:model/VehicleWithSummaryModel
     */
    constructor() { 
        
        VehicleWithSummaryModel.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>VehicleWithSummaryModel</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/VehicleWithSummaryModel} obj Optional instance to populate.
     * @return {module:model/VehicleWithSummaryModel} The populated <code>VehicleWithSummaryModel</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new VehicleWithSummaryModel();

            if (data.hasOwnProperty('vid')) {
                obj['vid'] = ApiClient.convertToType(data['vid'], 'Number');
            }
            if (data.hasOwnProperty('unitnr')) {
                obj['unitnr'] = ApiClient.convertToType(data['unitnr'], 'Number');
            }
            if (data.hasOwnProperty('registration')) {
                obj['registration'] = ApiClient.convertToType(data['registration'], 'String');
            }
            if (data.hasOwnProperty('make')) {
                obj['make'] = ApiClient.convertToType(data['make'], 'String');
            }
            if (data.hasOwnProperty('model')) {
                obj['model'] = ApiClient.convertToType(data['model'], 'String');
            }
            if (data.hasOwnProperty('version')) {
                obj['version'] = ApiClient.convertToType(data['version'], 'String');
            }
            if (data.hasOwnProperty('colour')) {
                obj['colour'] = ApiClient.convertToType(data['colour'], 'String');
            }
            if (data.hasOwnProperty('category')) {
                obj['category'] = ApiClient.convertToType(data['category'], 'String');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'String');
            }
            if (data.hasOwnProperty('epm')) {
                obj['epm'] = ApiClient.convertToType(data['epm'], 'Number');
            }
            if (data.hasOwnProperty('date_service_due')) {
                obj['date_service_due'] = ApiClient.convertToType(data['date_service_due'], 'String');
            }
            if (data.hasOwnProperty('due_off_kms')) {
                obj['due_off_kms'] = ApiClient.convertToType(data['due_off_kms'], 'String');
            }
            if (data.hasOwnProperty('custom_due_off_epm')) {
                obj['custom_due_off_epm'] = ApiClient.convertToType(data['custom_due_off_epm'], 'Number');
            }
            if (data.hasOwnProperty('custom_due_off_user')) {
                obj['custom_due_off_user'] = ApiClient.convertToType(data['custom_due_off_user'], 'String');
            }
            if (data.hasOwnProperty('created_by')) {
                obj['created_by'] = ApiClient.convertToType(data['created_by'], 'String');
            }
            if (data.hasOwnProperty('epm_due_off')) {
                obj['epm_due_off'] = ApiClient.convertToType(data['epm_due_off'], 'Number');
            }
            if (data.hasOwnProperty('fleet_type')) {
                obj['fleet_type'] = ApiClient.convertToType(data['fleet_type'], 'String');
            }
            if (data.hasOwnProperty('lat')) {
                obj['lat'] = ApiClient.convertToType(data['lat'], 'Number');
            }
            if (data.hasOwnProperty('lon')) {
                obj['lon'] = ApiClient.convertToType(data['lon'], 'Number');
            }
            if (data.hasOwnProperty('imei')) {
                obj['imei'] = ApiClient.convertToType(data['imei'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} vid
 */
VehicleWithSummaryModel.prototype['vid'] = undefined;

/**
 * @member {Number} unitnr
 */
VehicleWithSummaryModel.prototype['unitnr'] = undefined;

/**
 * @member {String} registration
 */
VehicleWithSummaryModel.prototype['registration'] = undefined;

/**
 * @member {String} make
 */
VehicleWithSummaryModel.prototype['make'] = undefined;

/**
 * @member {String} model
 */
VehicleWithSummaryModel.prototype['model'] = undefined;

/**
 * @member {String} version
 */
VehicleWithSummaryModel.prototype['version'] = undefined;

/**
 * @member {String} colour
 */
VehicleWithSummaryModel.prototype['colour'] = undefined;

/**
 * @member {String} category
 */
VehicleWithSummaryModel.prototype['category'] = undefined;

/**
 * @member {String} status
 */
VehicleWithSummaryModel.prototype['status'] = undefined;

/**
 * @member {Number} epm
 */
VehicleWithSummaryModel.prototype['epm'] = undefined;

/**
 * @member {String} date_service_due
 */
VehicleWithSummaryModel.prototype['date_service_due'] = undefined;

/**
 * @member {String} due_off_kms
 */
VehicleWithSummaryModel.prototype['due_off_kms'] = undefined;

/**
 * @member {Number} custom_due_off_epm
 */
VehicleWithSummaryModel.prototype['custom_due_off_epm'] = undefined;

/**
 * @member {String} custom_due_off_user
 */
VehicleWithSummaryModel.prototype['custom_due_off_user'] = undefined;

/**
 * @member {String} created_by
 */
VehicleWithSummaryModel.prototype['created_by'] = undefined;

/**
 * @member {Number} epm_due_off
 */
VehicleWithSummaryModel.prototype['epm_due_off'] = undefined;

/**
 * @member {String} fleet_type
 */
VehicleWithSummaryModel.prototype['fleet_type'] = undefined;

/**
 * @member {Number} lat
 */
VehicleWithSummaryModel.prototype['lat'] = undefined;

/**
 * @member {Number} lon
 */
VehicleWithSummaryModel.prototype['lon'] = undefined;

/**
 * @member {Number} imei
 */
VehicleWithSummaryModel.prototype['imei'] = undefined;






export default VehicleWithSummaryModel;

