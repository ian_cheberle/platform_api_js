/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RenterUserModel model module.
 * @module model/RenterUserModel
 * @version 1.0.0
 */
class RenterUserModel {
    /**
     * Constructs a new <code>RenterUserModel</code>.
     * @alias module:model/RenterUserModel
     */
    constructor() { 
        
        RenterUserModel.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RenterUserModel</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RenterUserModel} obj Optional instance to populate.
     * @return {module:model/RenterUserModel} The populated <code>RenterUserModel</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RenterUserModel();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('driver_id')) {
                obj['driver_id'] = ApiClient.convertToType(data['driver_id'], 'String');
            }
            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('email_verified_at')) {
                obj['email_verified_at'] = ApiClient.convertToType(data['email_verified_at'], 'String');
            }
            if (data.hasOwnProperty('account_type')) {
                obj['account_type'] = ApiClient.convertToType(data['account_type'], 'String');
            }
            if (data.hasOwnProperty('first_name')) {
                obj['first_name'] = ApiClient.convertToType(data['first_name'], 'String');
            }
            if (data.hasOwnProperty('surname')) {
                obj['surname'] = ApiClient.convertToType(data['surname'], 'String');
            }
            if (data.hasOwnProperty('address')) {
                obj['address'] = ApiClient.convertToType(data['address'], 'String');
            }
            if (data.hasOwnProperty('dob')) {
                obj['dob'] = ApiClient.convertToType(data['dob'], 'String');
            }
            if (data.hasOwnProperty('phone')) {
                obj['phone'] = ApiClient.convertToType(data['phone'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} id
 */
RenterUserModel.prototype['id'] = undefined;

/**
 * @member {String} driver_id
 */
RenterUserModel.prototype['driver_id'] = undefined;

/**
 * @member {String} email
 */
RenterUserModel.prototype['email'] = undefined;

/**
 * @member {String} email_verified_at
 */
RenterUserModel.prototype['email_verified_at'] = undefined;

/**
 * @member {String} account_type
 */
RenterUserModel.prototype['account_type'] = undefined;

/**
 * @member {String} first_name
 */
RenterUserModel.prototype['first_name'] = undefined;

/**
 * @member {String} surname
 */
RenterUserModel.prototype['surname'] = undefined;

/**
 * @member {String} address
 */
RenterUserModel.prototype['address'] = undefined;

/**
 * @member {String} dob
 */
RenterUserModel.prototype['dob'] = undefined;

/**
 * @member {String} phone
 */
RenterUserModel.prototype['phone'] = undefined;






export default RenterUserModel;

