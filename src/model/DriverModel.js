/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The DriverModel model module.
 * @module model/DriverModel
 * @version 1.0.0
 */
class DriverModel {
    /**
     * Constructs a new <code>DriverModel</code>.
     * @alias module:model/DriverModel
     */
    constructor() { 
        
        DriverModel.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>DriverModel</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/DriverModel} obj Optional instance to populate.
     * @return {module:model/DriverModel} The populated <code>DriverModel</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DriverModel();

            if (data.hasOwnProperty('first_name')) {
                obj['first_name'] = ApiClient.convertToType(data['first_name'], 'String');
            }
            if (data.hasOwnProperty('surname')) {
                obj['surname'] = ApiClient.convertToType(data['surname'], 'String');
            }
            if (data.hasOwnProperty('gender')) {
                obj['gender'] = ApiClient.convertToType(data['gender'], 'String');
            }
            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('dob')) {
                obj['dob'] = ApiClient.convertToType(data['dob'], 'String');
            }
            if (data.hasOwnProperty('phone_number')) {
                obj['phone_number'] = ApiClient.convertToType(data['phone_number'], 'String');
            }
            if (data.hasOwnProperty('driver_id')) {
                obj['driver_id'] = ApiClient.convertToType(data['driver_id'], 'String');
            }
            if (data.hasOwnProperty('group_id')) {
                obj['group_id'] = ApiClient.convertToType(data['group_id'], 'Number');
            }
            if (data.hasOwnProperty('nationality')) {
                obj['nationality'] = ApiClient.convertToType(data['nationality'], 'String');
            }
            if (data.hasOwnProperty('occupation')) {
                obj['occupation'] = ApiClient.convertToType(data['occupation'], 'String');
            }
            if (data.hasOwnProperty('safety_competition')) {
                obj['safety_competition'] = ApiClient.convertToType(data['safety_competition'], 'Boolean');
            }
            if (data.hasOwnProperty('email_customer_updates')) {
                obj['email_customer_updates'] = ApiClient.convertToType(data['email_customer_updates'], 'Boolean');
            }
        }
        return obj;
    }


}

/**
 * @member {String} first_name
 */
DriverModel.prototype['first_name'] = undefined;

/**
 * @member {String} surname
 */
DriverModel.prototype['surname'] = undefined;

/**
 * @member {String} gender
 */
DriverModel.prototype['gender'] = undefined;

/**
 * @member {String} email
 */
DriverModel.prototype['email'] = undefined;

/**
 * @member {String} dob
 */
DriverModel.prototype['dob'] = undefined;

/**
 * @member {String} phone_number
 */
DriverModel.prototype['phone_number'] = undefined;

/**
 * @member {String} driver_id
 */
DriverModel.prototype['driver_id'] = undefined;

/**
 * @member {Number} group_id
 */
DriverModel.prototype['group_id'] = undefined;

/**
 * @member {String} nationality
 */
DriverModel.prototype['nationality'] = undefined;

/**
 * @member {String} occupation
 */
DriverModel.prototype['occupation'] = undefined;

/**
 * @member {Boolean} safety_competition
 */
DriverModel.prototype['safety_competition'] = undefined;

/**
 * @member {Boolean} email_customer_updates
 */
DriverModel.prototype['email_customer_updates'] = undefined;






export default DriverModel;

