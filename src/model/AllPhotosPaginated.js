/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import PhotoModel from './PhotoModel';

/**
 * The AllPhotosPaginated model module.
 * @module model/AllPhotosPaginated
 * @version 1.0.0
 */
class AllPhotosPaginated {
    /**
     * Constructs a new <code>AllPhotosPaginated</code>.
     * @alias module:model/AllPhotosPaginated
     */
    constructor() { 
        
        AllPhotosPaginated.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>AllPhotosPaginated</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/AllPhotosPaginated} obj Optional instance to populate.
     * @return {module:model/AllPhotosPaginated} The populated <code>AllPhotosPaginated</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new AllPhotosPaginated();

            if (data.hasOwnProperty('current_page')) {
                obj['current_page'] = ApiClient.convertToType(data['current_page'], 'Number');
            }
            if (data.hasOwnProperty('first_page_url')) {
                obj['first_page_url'] = ApiClient.convertToType(data['first_page_url'], 'String');
            }
            if (data.hasOwnProperty('from')) {
                obj['from'] = ApiClient.convertToType(data['from'], 'Number');
            }
            if (data.hasOwnProperty('last_page')) {
                obj['last_page'] = ApiClient.convertToType(data['last_page'], 'Number');
            }
            if (data.hasOwnProperty('last_page_url')) {
                obj['last_page_url'] = ApiClient.convertToType(data['last_page_url'], 'String');
            }
            if (data.hasOwnProperty('next_page_url')) {
                obj['next_page_url'] = ApiClient.convertToType(data['next_page_url'], 'String');
            }
            if (data.hasOwnProperty('path')) {
                obj['path'] = ApiClient.convertToType(data['path'], 'String');
            }
            if (data.hasOwnProperty('per_page')) {
                obj['per_page'] = ApiClient.convertToType(data['per_page'], 'Number');
            }
            if (data.hasOwnProperty('prev_page_url')) {
                obj['prev_page_url'] = ApiClient.convertToType(data['prev_page_url'], 'String');
            }
            if (data.hasOwnProperty('to')) {
                obj['to'] = ApiClient.convertToType(data['to'], 'Number');
            }
            if (data.hasOwnProperty('total')) {
                obj['total'] = ApiClient.convertToType(data['total'], 'Number');
            }
            if (data.hasOwnProperty('data')) {
                obj['data'] = ApiClient.convertToType(data['data'], [PhotoModel]);
            }
        }
        return obj;
    }


}

/**
 * @member {Number} current_page
 */
AllPhotosPaginated.prototype['current_page'] = undefined;

/**
 * @member {String} first_page_url
 */
AllPhotosPaginated.prototype['first_page_url'] = undefined;

/**
 * @member {Number} from
 */
AllPhotosPaginated.prototype['from'] = undefined;

/**
 * @member {Number} last_page
 */
AllPhotosPaginated.prototype['last_page'] = undefined;

/**
 * @member {String} last_page_url
 */
AllPhotosPaginated.prototype['last_page_url'] = undefined;

/**
 * @member {String} next_page_url
 */
AllPhotosPaginated.prototype['next_page_url'] = undefined;

/**
 * @member {String} path
 */
AllPhotosPaginated.prototype['path'] = undefined;

/**
 * @member {Number} per_page
 */
AllPhotosPaginated.prototype['per_page'] = undefined;

/**
 * @member {String} prev_page_url
 */
AllPhotosPaginated.prototype['prev_page_url'] = undefined;

/**
 * @member {Number} to
 */
AllPhotosPaginated.prototype['to'] = undefined;

/**
 * @member {Number} total
 */
AllPhotosPaginated.prototype['total'] = undefined;

/**
 * @member {Array.<module:model/PhotoModel>} data
 */
AllPhotosPaginated.prototype['data'] = undefined;






export default AllPhotosPaginated;

