/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The VerifyBodyRequest model module.
 * @module model/VerifyBodyRequest
 * @version 1.0.0
 */
class VerifyBodyRequest {
    /**
     * Constructs a new <code>VerifyBodyRequest</code>.
     * @alias module:model/VerifyBodyRequest
     */
    constructor() { 
        
        VerifyBodyRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>VerifyBodyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/VerifyBodyRequest} obj Optional instance to populate.
     * @return {module:model/VerifyBodyRequest} The populated <code>VerifyBodyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new VerifyBodyRequest();

            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('token')) {
                obj['token'] = ApiClient.convertToType(data['token'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} email
 */
VerifyBodyRequest.prototype['email'] = undefined;

/**
 * @member {String} token
 */
VerifyBodyRequest.prototype['token'] = undefined;






export default VerifyBodyRequest;

