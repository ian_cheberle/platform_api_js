/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UpdateRenterRequest model module.
 * @module model/UpdateRenterRequest
 * @version 1.0.0
 */
class UpdateRenterRequest {
    /**
     * Constructs a new <code>UpdateRenterRequest</code>.
     * @alias module:model/UpdateRenterRequest
     */
    constructor() { 
        
        UpdateRenterRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UpdateRenterRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateRenterRequest} obj Optional instance to populate.
     * @return {module:model/UpdateRenterRequest} The populated <code>UpdateRenterRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdateRenterRequest();

            if (data.hasOwnProperty('driver_id')) {
                obj['driver_id'] = ApiClient.convertToType(data['driver_id'], 'String');
            }
            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('email_verified_at')) {
                obj['email_verified_at'] = ApiClient.convertToType(data['email_verified_at'], 'String');
            }
            if (data.hasOwnProperty('account_type')) {
                obj['account_type'] = ApiClient.convertToType(data['account_type'], 'String');
            }
            if (data.hasOwnProperty('first_name')) {
                obj['first_name'] = ApiClient.convertToType(data['first_name'], 'String');
            }
            if (data.hasOwnProperty('surname')) {
                obj['surname'] = ApiClient.convertToType(data['surname'], 'String');
            }
            if (data.hasOwnProperty('address')) {
                obj['address'] = ApiClient.convertToType(data['address'], 'String');
            }
            if (data.hasOwnProperty('dob')) {
                obj['dob'] = ApiClient.convertToType(data['dob'], 'String');
            }
            if (data.hasOwnProperty('phone')) {
                obj['phone'] = ApiClient.convertToType(data['phone'], 'String');
            }
            if (data.hasOwnProperty('notification_token')) {
                obj['notification_token'] = ApiClient.convertToType(data['notification_token'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} driver_id
 */
UpdateRenterRequest.prototype['driver_id'] = undefined;

/**
 * @member {String} email
 */
UpdateRenterRequest.prototype['email'] = undefined;

/**
 * @member {String} email_verified_at
 */
UpdateRenterRequest.prototype['email_verified_at'] = undefined;

/**
 * @member {String} account_type
 */
UpdateRenterRequest.prototype['account_type'] = undefined;

/**
 * @member {String} first_name
 */
UpdateRenterRequest.prototype['first_name'] = undefined;

/**
 * @member {String} surname
 */
UpdateRenterRequest.prototype['surname'] = undefined;

/**
 * @member {String} address
 */
UpdateRenterRequest.prototype['address'] = undefined;

/**
 * @member {String} dob
 */
UpdateRenterRequest.prototype['dob'] = undefined;

/**
 * @member {String} phone
 */
UpdateRenterRequest.prototype['phone'] = undefined;

/**
 * @member {String} notification_token
 */
UpdateRenterRequest.prototype['notification_token'] = undefined;






export default UpdateRenterRequest;

