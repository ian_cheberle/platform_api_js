/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateNRMAcceptableBodyRequest model module.
 * @module model/CreateNRMAcceptableBodyRequest
 * @version 1.0.0
 */
class CreateNRMAcceptableBodyRequest {
    /**
     * Constructs a new <code>CreateNRMAcceptableBodyRequest</code>.
     * @alias module:model/CreateNRMAcceptableBodyRequest
     */
    constructor() { 
        
        CreateNRMAcceptableBodyRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CreateNRMAcceptableBodyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateNRMAcceptableBodyRequest} obj Optional instance to populate.
     * @return {module:model/CreateNRMAcceptableBodyRequest} The populated <code>CreateNRMAcceptableBodyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateNRMAcceptableBodyRequest();

            if (data.hasOwnProperty('end_timestamp')) {
                obj['end_timestamp'] = ApiClient.convertToType(data['end_timestamp'], 'String');
            }
            if (data.hasOwnProperty('start_timestamp')) {
                obj['start_timestamp'] = ApiClient.convertToType(data['start_timestamp'], 'String');
            }
            if (data.hasOwnProperty('nrm_number')) {
                obj['nrm_number'] = ApiClient.convertToType(data['nrm_number'], 'String');
            }
            if (data.hasOwnProperty('reason')) {
                obj['reason'] = ApiClient.convertToType(data['reason'], 'String');
            }
            if (data.hasOwnProperty('registration')) {
                obj['registration'] = ApiClient.convertToType(data['registration'], 'String');
            }
            if (data.hasOwnProperty('rs_odometer_in')) {
                obj['rs_odometer_in'] = ApiClient.convertToType(data['rs_odometer_in'], 'Number');
            }
            if (data.hasOwnProperty('rs_odometer_out')) {
                obj['rs_odometer_out'] = ApiClient.convertToType(data['rs_odometer_out'], 'Number');
            }
            if (data.hasOwnProperty('vid')) {
                obj['vid'] = ApiClient.convertToType(data['vid'], 'String');
            }
            if (data.hasOwnProperty('station_out')) {
                obj['station_out'] = ApiClient.convertToType(data['station_out'], 'String');
            }
            if (data.hasOwnProperty('station_in')) {
                obj['station_in'] = ApiClient.convertToType(data['station_in'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} end_timestamp
 */
CreateNRMAcceptableBodyRequest.prototype['end_timestamp'] = undefined;

/**
 * @member {String} start_timestamp
 */
CreateNRMAcceptableBodyRequest.prototype['start_timestamp'] = undefined;

/**
 * @member {String} nrm_number
 */
CreateNRMAcceptableBodyRequest.prototype['nrm_number'] = undefined;

/**
 * @member {String} reason
 */
CreateNRMAcceptableBodyRequest.prototype['reason'] = undefined;

/**
 * @member {String} registration
 */
CreateNRMAcceptableBodyRequest.prototype['registration'] = undefined;

/**
 * @member {Number} rs_odometer_in
 */
CreateNRMAcceptableBodyRequest.prototype['rs_odometer_in'] = undefined;

/**
 * @member {Number} rs_odometer_out
 */
CreateNRMAcceptableBodyRequest.prototype['rs_odometer_out'] = undefined;

/**
 * @member {String} vid
 */
CreateNRMAcceptableBodyRequest.prototype['vid'] = undefined;

/**
 * @member {String} station_out
 */
CreateNRMAcceptableBodyRequest.prototype['station_out'] = undefined;

/**
 * @member {String} station_in
 */
CreateNRMAcceptableBodyRequest.prototype['station_in'] = undefined;






export default CreateNRMAcceptableBodyRequest;

