/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateStationAcceptableBodyRequest model module.
 * @module model/CreateStationAcceptableBodyRequest
 * @version 1.0.0
 */
class CreateStationAcceptableBodyRequest {
    /**
     * Constructs a new <code>CreateStationAcceptableBodyRequest</code>.
     * @alias module:model/CreateStationAcceptableBodyRequest
     */
    constructor() { 
        
        CreateStationAcceptableBodyRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CreateStationAcceptableBodyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateStationAcceptableBodyRequest} obj Optional instance to populate.
     * @return {module:model/CreateStationAcceptableBodyRequest} The populated <code>CreateStationAcceptableBodyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateStationAcceptableBodyRequest();

            if (data.hasOwnProperty('station_id')) {
                obj['station_id'] = ApiClient.convertToType(data['station_id'], 'String');
            }
            if (data.hasOwnProperty('display_name')) {
                obj['display_name'] = ApiClient.convertToType(data['display_name'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} station_id
 */
CreateStationAcceptableBodyRequest.prototype['station_id'] = undefined;

/**
 * @member {String} display_name
 */
CreateStationAcceptableBodyRequest.prototype['display_name'] = undefined;






export default CreateStationAcceptableBodyRequest;

