/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UpdateDeviceAcceptableBodyRequest model module.
 * @module model/UpdateDeviceAcceptableBodyRequest
 * @version 1.0.0
 */
class UpdateDeviceAcceptableBodyRequest {
    /**
     * Constructs a new <code>UpdateDeviceAcceptableBodyRequest</code>.
     * @alias module:model/UpdateDeviceAcceptableBodyRequest
     */
    constructor() { 
        
        UpdateDeviceAcceptableBodyRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UpdateDeviceAcceptableBodyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateDeviceAcceptableBodyRequest} obj Optional instance to populate.
     * @return {module:model/UpdateDeviceAcceptableBodyRequest} The populated <code>UpdateDeviceAcceptableBodyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdateDeviceAcceptableBodyRequest();

            if (data.hasOwnProperty('msisdn')) {
                obj['msisdn'] = ApiClient.convertToType(data['msisdn'], 'Number');
            }
            if (data.hasOwnProperty('iccid')) {
                obj['iccid'] = ApiClient.convertToType(data['iccid'], 'Number');
            }
            if (data.hasOwnProperty('manufacturer')) {
                obj['manufacturer'] = ApiClient.convertToType(data['manufacturer'], 'String');
            }
            if (data.hasOwnProperty('model')) {
                obj['model'] = ApiClient.convertToType(data['model'], 'String');
            }
            if (data.hasOwnProperty('url')) {
                obj['url'] = ApiClient.convertToType(data['url'], 'String');
            }
            if (data.hasOwnProperty('network')) {
                obj['network'] = ApiClient.convertToType(data['network'], 'String');
            }
            if (data.hasOwnProperty('purchased_date')) {
                obj['purchased_date'] = ApiClient.convertToType(data['purchased_date'], 'Number');
            }
            if (data.hasOwnProperty('mxp')) {
                obj['mxp'] = ApiClient.convertToType(data['mxp'], 'Number');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} msisdn
 */
UpdateDeviceAcceptableBodyRequest.prototype['msisdn'] = undefined;

/**
 * @member {Number} iccid
 */
UpdateDeviceAcceptableBodyRequest.prototype['iccid'] = undefined;

/**
 * @member {String} manufacturer
 */
UpdateDeviceAcceptableBodyRequest.prototype['manufacturer'] = undefined;

/**
 * @member {String} model
 */
UpdateDeviceAcceptableBodyRequest.prototype['model'] = undefined;

/**
 * @member {String} url
 */
UpdateDeviceAcceptableBodyRequest.prototype['url'] = undefined;

/**
 * @member {String} network
 */
UpdateDeviceAcceptableBodyRequest.prototype['network'] = undefined;

/**
 * @member {Number} purchased_date
 */
UpdateDeviceAcceptableBodyRequest.prototype['purchased_date'] = undefined;

/**
 * @member {Number} mxp
 */
UpdateDeviceAcceptableBodyRequest.prototype['mxp'] = undefined;

/**
 * @member {String} status
 */
UpdateDeviceAcceptableBodyRequest.prototype['status'] = undefined;






export default UpdateDeviceAcceptableBodyRequest;

