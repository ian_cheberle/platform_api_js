/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RentalSummaryModel model module.
 * @module model/RentalSummaryModel
 * @version 1.0.0
 */
class RentalSummaryModel {
    /**
     * Constructs a new <code>RentalSummaryModel</code>.
     * @alias module:model/RentalSummaryModel
     */
    constructor() { 
        
        RentalSummaryModel.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>RentalSummaryModel</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RentalSummaryModel} obj Optional instance to populate.
     * @return {module:model/RentalSummaryModel} The populated <code>RentalSummaryModel</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RentalSummaryModel();

            if (data.hasOwnProperty('station_out')) {
                obj['station_out'] = ApiClient.convertToType(data['station_out'], 'String');
            }
            if (data.hasOwnProperty('start_timestamp')) {
                obj['start_timestamp'] = ApiClient.convertToType(data['start_timestamp'], 'Number');
            }
            if (data.hasOwnProperty('end_timestamp')) {
                obj['end_timestamp'] = ApiClient.convertToType(data['end_timestamp'], 'Number');
            }
            if (data.hasOwnProperty('imeis')) {
                obj['imeis'] = ApiClient.convertToType(data['imeis'], 'String');
            }
            if (data.hasOwnProperty('odometers')) {
                obj['odometers'] = ApiClient.convertToType(data['odometers'], 'String');
            }
            if (data.hasOwnProperty('ra_number')) {
                obj['ra_number'] = ApiClient.convertToType(data['ra_number'], 'String');
            }
            if (data.hasOwnProperty('registrations')) {
                obj['registrations'] = ApiClient.convertToType(data['registrations'], 'String');
            }
            if (data.hasOwnProperty('rental_status')) {
                obj['rental_status'] = ApiClient.convertToType(data['rental_status'], 'String');
            }
            if (data.hasOwnProperty('vids')) {
                obj['vids'] = ApiClient.convertToType(data['vids'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {String} station_out
 */
RentalSummaryModel.prototype['station_out'] = undefined;

/**
 * @member {Number} start_timestamp
 */
RentalSummaryModel.prototype['start_timestamp'] = undefined;

/**
 * @member {Number} end_timestamp
 */
RentalSummaryModel.prototype['end_timestamp'] = undefined;

/**
 * @member {String} imeis
 */
RentalSummaryModel.prototype['imeis'] = undefined;

/**
 * @member {String} odometers
 */
RentalSummaryModel.prototype['odometers'] = undefined;

/**
 * @member {String} ra_number
 */
RentalSummaryModel.prototype['ra_number'] = undefined;

/**
 * @member {String} registrations
 */
RentalSummaryModel.prototype['registrations'] = undefined;

/**
 * @member {String} rental_status
 */
RentalSummaryModel.prototype['rental_status'] = undefined;

/**
 * @member {String} vids
 */
RentalSummaryModel.prototype['vids'] = undefined;






export default RentalSummaryModel;

