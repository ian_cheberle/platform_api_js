/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AllDriversPaginated from '../model/AllDriversPaginated';
import AnyType from '../model/AnyType';
import CreateDriverAcceptableBodyRequest from '../model/CreateDriverAcceptableBodyRequest';
import DeviceAlertDTO from '../model/DeviceAlertDTO';
import DriverModel from '../model/DriverModel';
import GeoAlertDTO from '../model/GeoAlertDTO';
import IdleEventDTO from '../model/IdleEventDTO';
import ImpactDTO from '../model/ImpactDTO';
import InlineResponse2011 from '../model/InlineResponse2011';
import JourneyDTO from '../model/JourneyDTO';
import PositionDTO from '../model/PositionDTO';
import RentalModel from '../model/RentalModel';
import SpeedEventDTO from '../model/SpeedEventDTO';
import UpdateDriverAcceptableBodyRequest from '../model/UpdateDriverAcceptableBodyRequest';

/**
* Drivers service.
* @module api/DriversApi
* @version 1.0.0
*/
export default class DriversApi {

    /**
    * Constructs a new DriversApi. 
    * @alias module:api/DriversApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerCreateDriver operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerCreateDriverCallback
     * @param {String} error Error message, if any.
     * @param {module:model/InlineResponse2011} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a single Driver
     * @param {String} tenant Origin of the request (Company)
     * @param {module:model/CreateDriverAcceptableBodyRequest} createDriverAcceptableBodyRequest Driver object that needs to be added to the platform
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerCreateDriverCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/InlineResponse2011}
     */
    modulesDriverHttpControllersDriverControllerCreateDriver(tenant, createDriverAcceptableBodyRequest, callback) {
      let postBody = createDriverAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerCreateDriver");
      }
      // verify the required parameter 'createDriverAcceptableBodyRequest' is set
      if (createDriverAcceptableBodyRequest === undefined || createDriverAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'createDriverAcceptableBodyRequest' when calling modulesDriverHttpControllersDriverControllerCreateDriver");
      }

      let pathParams = {
        'tenant': tenant
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = InlineResponse2011;
      return this.apiClient.callApi(
        '/{tenant}/drivers', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerDeleteDriver operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerDeleteDriverCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Deletes a single Driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerDeleteDriverCallback} callback The callback function, accepting three arguments: error, data, response
     */
    modulesDriverHttpControllersDriverControllerDeleteDriver(tenant, driverId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerDeleteDriver");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerDeleteDriver");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = [];
      let returnType = null;
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDeviceAlerts operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDeviceAlertsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/DeviceAlertDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all device alerts based on the driver id
     * Returns device alerts data
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDeviceAlertsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/DeviceAlertDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDeviceAlerts(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDeviceAlerts");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDeviceAlerts");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [DeviceAlertDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/devicealerts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriver operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverCallback
     * @param {String} error Error message, if any.
     * @param {module:model/DriverModel} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Finds a single driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/DriverModel}
     */
    modulesDriverHttpControllersDriverControllerGetDriver(tenant, driverId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriver");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriver");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = DriverModel;
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverGeoAlertsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/GeoAlertDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all geo alerts based on a specific driver
     * Returns a driver geo alerts data
     * @param {String} tenant Origin of the request (Driver)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverGeoAlertsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/GeoAlertDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [GeoAlertDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/geoalerts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverIdleEvents operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverIdleEventsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/IdleEventDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all idle events based on a specific driver
     * Get all device idle events of a Driver
     * @param {String} tenant Origin of the request (Driver)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverIdleEventsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/IdleEventDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverIdleEvents(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverIdleEvents");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverIdleEvents");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [IdleEventDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/idleevents', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverImpacts operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverImpactsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/ImpactDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all impacts based on a specific driver
     * Returns a driver impacts data
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverImpactsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/ImpactDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverImpacts(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverImpacts");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverImpacts");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [ImpactDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/impacts', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverJourneys operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverJourneysCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/JourneyDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all journeys based on a specific driver
     * Get all journeys of a Driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverJourneysCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/JourneyDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverJourneys(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverJourneys");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverJourneys");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [JourneyDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/journeys', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverPosition operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverPositionCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/PositionDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get latest position for a specific driver
     * Return latest position for the driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverPositionCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/PositionDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverPosition(tenant, driverId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverPosition");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverPosition");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [PositionDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/position', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverRentals operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverRentalsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/RentalModel>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all rentals based on a specific driver
     * Get all rentals of a Driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverRentalsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/RentalModel>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverRentals(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverRentals");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverRentals");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [RentalModel];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/rentals', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverSpeedEventsCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/SpeedEventDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all speed events based on a specific driver
     * Get all device speed events of a Driver
     * @param {String} tenant Origin of the request (Driver)
     * @param {String} driverId Driver Id
     * @param {Object} opts Optional parameters
     * @param {Number} opts.limit 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriverSpeedEventsCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/SpeedEventDTO>}
     */
    modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents(tenant, driverId, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
        'limit': opts['limit']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [SpeedEventDTO];
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}/speedevents', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerGetDrivers operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriversCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AllDriversPaginated} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all Drivers
     * Returns driver data paginated
     * @param {String} tenant Origin of the request (Company)
     * @param {Object} opts Optional parameters
     * @param {String} opts.groupId 
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerGetDriversCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AllDriversPaginated}
     */
    modulesDriverHttpControllersDriverControllerGetDrivers(tenant, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerGetDrivers");
      }

      let pathParams = {
        'tenant': tenant
      };
      let queryParams = {
        'group_id': opts['groupId']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AllDriversPaginated;
      return this.apiClient.callApi(
        '/{tenant}/drivers', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesDriverHttpControllersDriverControllerUpdateDriver operation.
     * @callback module:api/DriversApi~modulesDriverHttpControllersDriverControllerUpdateDriverCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AnyType} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update an specific Driver
     * @param {String} tenant Origin of the request (Company)
     * @param {String} driverId Driver Id
     * @param {module:model/UpdateDriverAcceptableBodyRequest} updateDriverAcceptableBodyRequest Driver values to be updated
     * @param {module:api/DriversApi~modulesDriverHttpControllersDriverControllerUpdateDriverCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AnyType}
     */
    modulesDriverHttpControllersDriverControllerUpdateDriver(tenant, driverId, updateDriverAcceptableBodyRequest, callback) {
      let postBody = updateDriverAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesDriverHttpControllersDriverControllerUpdateDriver");
      }
      // verify the required parameter 'driverId' is set
      if (driverId === undefined || driverId === null) {
        throw new Error("Missing the required parameter 'driverId' when calling modulesDriverHttpControllersDriverControllerUpdateDriver");
      }
      // verify the required parameter 'updateDriverAcceptableBodyRequest' is set
      if (updateDriverAcceptableBodyRequest === undefined || updateDriverAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'updateDriverAcceptableBodyRequest' when calling modulesDriverHttpControllersDriverControllerUpdateDriver");
      }

      let pathParams = {
        'tenant': tenant,
        'driver_id': driverId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = AnyType;
      return this.apiClient.callApi(
        '/{tenant}/drivers/{driver_id}', 'PATCH',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
