/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AllPhotosPaginated from '../model/AllPhotosPaginated';
import AnyType from '../model/AnyType';
import CreatePhotoAcceptableBodyRequest from '../model/CreatePhotoAcceptableBodyRequest';
import InlineResponse201 from '../model/InlineResponse201';
import PhotoModel from '../model/PhotoModel';
import UpdatePhotoAcceptableBodyRequest from '../model/UpdatePhotoAcceptableBodyRequest';

/**
* Photos service.
* @module api/PhotosApi
* @version 1.0.0
*/
export default class PhotosApi {

    /**
    * Constructs a new PhotosApi. 
    * @alias module:api/PhotosApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the modulesPhotoHttpControllersPhotoControllerCreatePhoto operation.
     * @callback module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerCreatePhotoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/InlineResponse201} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a single photo
     * @param {String} tenant Origin of the request (Company)
     * @param {module:model/CreatePhotoAcceptableBodyRequest} createPhotoAcceptableBodyRequest Photo object that needs to be added to the platform
     * @param {module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerCreatePhotoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/InlineResponse201}
     */
    modulesPhotoHttpControllersPhotoControllerCreatePhoto(tenant, createPhotoAcceptableBodyRequest, callback) {
      let postBody = createPhotoAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesPhotoHttpControllersPhotoControllerCreatePhoto");
      }
      // verify the required parameter 'createPhotoAcceptableBodyRequest' is set
      if (createPhotoAcceptableBodyRequest === undefined || createPhotoAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'createPhotoAcceptableBodyRequest' when calling modulesPhotoHttpControllersPhotoControllerCreatePhoto");
      }

      let pathParams = {
        'tenant': tenant
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = InlineResponse201;
      return this.apiClient.callApi(
        '/{tenant}/counterless/photos', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesPhotoHttpControllersPhotoControllerDeletePhoto operation.
     * @callback module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerDeletePhotoCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Deletes a single Photo
     * @param {String} tenant Origin of the request (Company)
     * @param {String} photoId Photo Id
     * @param {module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerDeletePhotoCallback} callback The callback function, accepting three arguments: error, data, response
     */
    modulesPhotoHttpControllersPhotoControllerDeletePhoto(tenant, photoId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesPhotoHttpControllersPhotoControllerDeletePhoto");
      }
      // verify the required parameter 'photoId' is set
      if (photoId === undefined || photoId === null) {
        throw new Error("Missing the required parameter 'photoId' when calling modulesPhotoHttpControllersPhotoControllerDeletePhoto");
      }

      let pathParams = {
        'tenant': tenant,
        'photo_id': photoId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = [];
      let returnType = null;
      return this.apiClient.callApi(
        '/{tenant}/counterless/photos/{photo_id}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesPhotoHttpControllersPhotoControllerGetPhoto operation.
     * @callback module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerGetPhotoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/PhotoModel} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Finds a single photo
     * @param {String} tenant Origin of the request (Company)
     * @param {Number} photoId Photo Id
     * @param {module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerGetPhotoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/PhotoModel}
     */
    modulesPhotoHttpControllersPhotoControllerGetPhoto(tenant, photoId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesPhotoHttpControllersPhotoControllerGetPhoto");
      }
      // verify the required parameter 'photoId' is set
      if (photoId === undefined || photoId === null) {
        throw new Error("Missing the required parameter 'photoId' when calling modulesPhotoHttpControllersPhotoControllerGetPhoto");
      }

      let pathParams = {
        'tenant': tenant,
        'photo_id': photoId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = PhotoModel;
      return this.apiClient.callApi(
        '/{tenant}/counterless/photos/{photo_id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesPhotoHttpControllersPhotoControllerGetPhotos operation.
     * @callback module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerGetPhotosCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AllPhotosPaginated} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all Photos
     * Returns photos data paginated
     * @param {String} tenant Origin of the request (Company)
     * @param {Object} opts Optional parameters
     * @param {String} opts.driverId 
     * @param {String} opts.licenceId 
     * @param {String} opts.rentalId 
     * @param {String} opts.type 
     * @param {Number} opts.perPage Number of items per page
     * @param {Number} opts.page Current page
     * @param {module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerGetPhotosCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AllPhotosPaginated}
     */
    modulesPhotoHttpControllersPhotoControllerGetPhotos(tenant, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesPhotoHttpControllersPhotoControllerGetPhotos");
      }

      let pathParams = {
        'tenant': tenant
      };
      let queryParams = {
        'driver_id': opts['driverId'],
        'licence_id': opts['licenceId'],
        'rental_id': opts['rentalId'],
        'type': opts['type'],
        'per_page': opts['perPage'],
        'page': opts['page']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AllPhotosPaginated;
      return this.apiClient.callApi(
        '/{tenant}/counterless/photos', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesPhotoHttpControllersPhotoControllerUpdatePhoto operation.
     * @callback module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerUpdatePhotoCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AnyType} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update an specific Photo
     * @param {String} tenant Origin of the request (Company)
     * @param {Number} photoId Photo Id
     * @param {module:model/UpdatePhotoAcceptableBodyRequest} updatePhotoAcceptableBodyRequest Photo values to be updated
     * @param {module:api/PhotosApi~modulesPhotoHttpControllersPhotoControllerUpdatePhotoCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AnyType}
     */
    modulesPhotoHttpControllersPhotoControllerUpdatePhoto(tenant, photoId, updatePhotoAcceptableBodyRequest, callback) {
      let postBody = updatePhotoAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesPhotoHttpControllersPhotoControllerUpdatePhoto");
      }
      // verify the required parameter 'photoId' is set
      if (photoId === undefined || photoId === null) {
        throw new Error("Missing the required parameter 'photoId' when calling modulesPhotoHttpControllersPhotoControllerUpdatePhoto");
      }
      // verify the required parameter 'updatePhotoAcceptableBodyRequest' is set
      if (updatePhotoAcceptableBodyRequest === undefined || updatePhotoAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'updatePhotoAcceptableBodyRequest' when calling modulesPhotoHttpControllersPhotoControllerUpdatePhoto");
      }

      let pathParams = {
        'tenant': tenant,
        'photo_id': photoId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = AnyType;
      return this.apiClient.callApi(
        '/{tenant}/counterless/photos/{photo_id}', 'PATCH',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
