/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import AllLicencesPaginated from '../model/AllLicencesPaginated';
import AnyType from '../model/AnyType';
import CreateLicenceAcceptableBodyRequest from '../model/CreateLicenceAcceptableBodyRequest';
import InlineResponse201 from '../model/InlineResponse201';
import InlineResponse422 from '../model/InlineResponse422';
import LicenceModel from '../model/LicenceModel';
import UpdateLicenceAcceptableBodyRequest from '../model/UpdateLicenceAcceptableBodyRequest';

/**
* Licence service.
* @module api/LicenceApi
* @version 1.0.0
*/
export default class LicenceApi {

    /**
    * Constructs a new LicenceApi. 
    * @alias module:api/LicenceApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence operation.
     * @callback module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicenceCallback
     * @param {String} error Error message, if any.
     * @param {module:model/InlineResponse201} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Creates a new Licence
     * @param {String} tenant Origin of the request (Company)
     * @param {String} renterId Renter whose data should be updated
     * @param {module:model/CreateLicenceAcceptableBodyRequest} createLicenceAcceptableBodyRequest Licence object that needs to be added to the platform
     * @param {module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicenceCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/InlineResponse201}
     */
    modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence(tenant, renterId, createLicenceAcceptableBodyRequest, callback) {
      let postBody = createLicenceAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence");
      }
      // verify the required parameter 'renterId' is set
      if (renterId === undefined || renterId === null) {
        throw new Error("Missing the required parameter 'renterId' when calling modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence");
      }
      // verify the required parameter 'createLicenceAcceptableBodyRequest' is set
      if (createLicenceAcceptableBodyRequest === undefined || createLicenceAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'createLicenceAcceptableBodyRequest' when calling modulesRenterUserHttpControllersLicenceLicenceControllerCreateLicence");
      }

      let pathParams = {
        'tenant': tenant,
        'renterId': renterId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = InlineResponse201;
      return this.apiClient.callApi(
        '/{tenant}/counterless/renters/{renterId}/licence', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence operation.
     * @callback module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicenceCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AnyType} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Deletes a Licence by Renter Id
     * Delete a Licence
     * @param {String} tenant Origin of the request (Company)
     * @param {String} renterId Renter of the request (Driver Id)
     * @param {module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicenceCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AnyType}
     */
    modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence(tenant, renterId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence");
      }
      // verify the required parameter 'renterId' is set
      if (renterId === undefined || renterId === null) {
        throw new Error("Missing the required parameter 'renterId' when calling modulesRenterUserHttpControllersLicenceLicenceControllerDeleteLicence");
      }

      let pathParams = {
        'tenant': tenant,
        'renterId': renterId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AnyType;
      return this.apiClient.callApi(
        '/{tenant}/counterless/renters/{renterId}/licence', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence operation.
     * @callback module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerGetLicenceCallback
     * @param {String} error Error message, if any.
     * @param {module:model/LicenceModel} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Gets Renter licence
     * @param {String} tenant Origin of the request (Company)
     * @param {String} renterId Renter Id
     * @param {module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerGetLicenceCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/LicenceModel}
     */
    modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence(tenant, renterId, callback) {
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence");
      }
      // verify the required parameter 'renterId' is set
      if (renterId === undefined || renterId === null) {
        throw new Error("Missing the required parameter 'renterId' when calling modulesRenterUserHttpControllersLicenceLicenceControllerGetLicence");
      }

      let pathParams = {
        'tenant': tenant,
        'renterId': renterId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = LicenceModel;
      return this.apiClient.callApi(
        '/{tenant}/counterless/renters/{renterId}/licence', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences operation.
     * @callback module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerGetLicencesCallback
     * @param {String} error Error message, if any.
     * @param {module:model/AllLicencesPaginated} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all Licences
     * Returns Licence data paginated
     * @param {String} tenant Origin of the request (Company)
     * @param {Object} opts Optional parameters
     * @param {String} opts.driverId 
     * @param {Boolean} opts.licenceApproved 
     * @param {String} opts.licenceApprovedUser 
     * @param {Boolean} opts.licenceRejected 
     * @param {String} opts.licenceRejectedUser 
     * @param {Number} opts.perPage Number of items per page
     * @param {Number} opts.page Current page
     * @param {module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerGetLicencesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/AllLicencesPaginated}
     */
    modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences(tenant, opts, callback) {
      opts = opts || {};
      let postBody = null;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesRenterUserHttpControllersLicenceLicenceControllerGetLicences");
      }

      let pathParams = {
        'tenant': tenant
      };
      let queryParams = {
        'driver_id': opts['driverId'],
        'licence_approved': opts['licenceApproved'],
        'licence_approved_user': opts['licenceApprovedUser'],
        'licence_rejected': opts['licenceRejected'],
        'licence_rejected_user': opts['licenceRejectedUser'],
        'per_page': opts['perPage'],
        'page': opts['page']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = AllLicencesPaginated;
      return this.apiClient.callApi(
        '/{tenant}/counterless/licences', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence operation.
     * @callback module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicenceCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Update a Renter licence
     * @param {String} tenant Origin of the request (Company)
     * @param {String} renterId Renter whose data should be updated
     * @param {module:model/UpdateLicenceAcceptableBodyRequest} updateLicenceAcceptableBodyRequest Licence object that needs to be updated to the platform
     * @param {module:api/LicenceApi~modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicenceCallback} callback The callback function, accepting three arguments: error, data, response
     */
    modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence(tenant, renterId, updateLicenceAcceptableBodyRequest, callback) {
      let postBody = updateLicenceAcceptableBodyRequest;
      // verify the required parameter 'tenant' is set
      if (tenant === undefined || tenant === null) {
        throw new Error("Missing the required parameter 'tenant' when calling modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence");
      }
      // verify the required parameter 'renterId' is set
      if (renterId === undefined || renterId === null) {
        throw new Error("Missing the required parameter 'renterId' when calling modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence");
      }
      // verify the required parameter 'updateLicenceAcceptableBodyRequest' is set
      if (updateLicenceAcceptableBodyRequest === undefined || updateLicenceAcceptableBodyRequest === null) {
        throw new Error("Missing the required parameter 'updateLicenceAcceptableBodyRequest' when calling modulesRenterUserHttpControllersLicenceLicenceControllerUpdateLicence");
      }

      let pathParams = {
        'tenant': tenant,
        'renterId': renterId
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['bearerAuth'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = null;
      return this.apiClient.callApi(
        '/{tenant}/counterless/renters/{renterId}/licence', 'PATCH',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
