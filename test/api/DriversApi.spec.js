/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PlatformApi);
  }
}(this, function(expect, PlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new PlatformApi.DriversApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DriversApi', function() {
    describe('modulesDriverHttpControllersDriverControllerCreateDriver', function() {
      it('should call modulesDriverHttpControllersDriverControllerCreateDriver successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerCreateDriver
        //instance.modulesDriverHttpControllersDriverControllerCreateDriver(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerDeleteDriver', function() {
      it('should call modulesDriverHttpControllersDriverControllerDeleteDriver successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerDeleteDriver
        //instance.modulesDriverHttpControllersDriverControllerDeleteDriver(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDeviceAlerts', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDeviceAlerts successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDeviceAlerts
        //instance.modulesDriverHttpControllersDriverControllerGetDeviceAlerts(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriver', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriver successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriver
        //instance.modulesDriverHttpControllersDriverControllerGetDriver(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts
        //instance.modulesDriverHttpControllersDriverControllerGetDriverGeoAlerts(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverIdleEvents', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverIdleEvents successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverIdleEvents
        //instance.modulesDriverHttpControllersDriverControllerGetDriverIdleEvents(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverImpacts', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverImpacts successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverImpacts
        //instance.modulesDriverHttpControllersDriverControllerGetDriverImpacts(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverJourneys', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverJourneys successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverJourneys
        //instance.modulesDriverHttpControllersDriverControllerGetDriverJourneys(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverPosition', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverPosition successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverPosition
        //instance.modulesDriverHttpControllersDriverControllerGetDriverPosition(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverRentals', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverRentals successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverRentals
        //instance.modulesDriverHttpControllersDriverControllerGetDriverRentals(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents
        //instance.modulesDriverHttpControllersDriverControllerGetDriverSpeedEvents(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerGetDrivers', function() {
      it('should call modulesDriverHttpControllersDriverControllerGetDrivers successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerGetDrivers
        //instance.modulesDriverHttpControllersDriverControllerGetDrivers(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('modulesDriverHttpControllersDriverControllerUpdateDriver', function() {
      it('should call modulesDriverHttpControllersDriverControllerUpdateDriver successfully', function(done) {
        //uncomment below and update the code to test modulesDriverHttpControllersDriverControllerUpdateDriver
        //instance.modulesDriverHttpControllersDriverControllerUpdateDriver(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
