/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PlatformApi);
  }
}(this, function(expect, PlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new PlatformApi.InlineObject();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('InlineObject', function() {
    it('should create an instance of InlineObject', function() {
      // uncomment below and update the code to test InlineObject
      //var instane = new PlatformApi.InlineObject();
      //expect(instance).to.be.a(PlatformApi.InlineObject);
    });

    it('should have the property email (base name: "email")', function() {
      // uncomment below and update the code to test the property email
      //var instane = new PlatformApi.InlineObject();
      //expect(instance).to.be();
    });

    it('should have the property password (base name: "password")', function() {
      // uncomment below and update the code to test the property password
      //var instane = new PlatformApi.InlineObject();
      //expect(instance).to.be();
    });

    it('should have the property acceptedTerms (base name: "accepted_terms")', function() {
      // uncomment below and update the code to test the property acceptedTerms
      //var instane = new PlatformApi.InlineObject();
      //expect(instance).to.be();
    });

  });

}));
