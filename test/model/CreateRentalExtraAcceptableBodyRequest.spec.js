/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PlatformApi);
  }
}(this, function(expect, PlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new PlatformApi.CreateRentalExtraAcceptableBodyRequest();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('CreateRentalExtraAcceptableBodyRequest', function() {
    it('should create an instance of CreateRentalExtraAcceptableBodyRequest', function() {
      // uncomment below and update the code to test CreateRentalExtraAcceptableBodyRequest
      //var instane = new PlatformApi.CreateRentalExtraAcceptableBodyRequest();
      //expect(instance).to.be.a(PlatformApi.CreateRentalExtraAcceptableBodyRequest);
    });

    it('should have the property extrasDesc (base name: "extras_desc")', function() {
      // uncomment below and update the code to test the property extrasDesc
      //var instane = new PlatformApi.CreateRentalExtraAcceptableBodyRequest();
      //expect(instance).to.be();
    });

  });

}));
