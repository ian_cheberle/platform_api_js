/**
 * Platform API
 * Open API for our frontend and internal services.
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: support@rentalmatics.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.PlatformApi);
  }
}(this, function(expect, PlatformApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new PlatformApi.JourneyModel();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('JourneyModel', function() {
    it('should create an instance of JourneyModel', function() {
      // uncomment below and update the code to test JourneyModel
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be.a(PlatformApi.JourneyModel);
    });

    it('should have the property imei (base name: "imei")', function() {
      // uncomment below and update the code to test the property imei
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property startTimestamp (base name: "start_timestamp")', function() {
      // uncomment below and update the code to test the property startTimestamp
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property endTimestamp (base name: "end_timestamp")', function() {
      // uncomment below and update the code to test the property endTimestamp
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property distance (base name: "distance")', function() {
      // uncomment below and update the code to test the property distance
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property duration (base name: "duration")', function() {
      // uncomment below and update the code to test the property duration
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property averageSpeed (base name: "average_speed")', function() {
      // uncomment below and update the code to test the property averageSpeed
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property idleDuration (base name: "idle_duration")', function() {
      // uncomment below and update the code to test the property idleDuration
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreFatigue (base name: "score_fatigue")', function() {
      // uncomment below and update the code to test the property scoreFatigue
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreIdle (base name: "score_idle")', function() {
      // uncomment below and update the code to test the property scoreIdle
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreOverall (base name: "score_overall")', function() {
      // uncomment below and update the code to test the property scoreOverall
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreSpeed (base name: "score_speed")', function() {
      // uncomment below and update the code to test the property scoreSpeed
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreSmooth (base name: "score_smooth")', function() {
      // uncomment below and update the code to test the property scoreSmooth
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

    it('should have the property scoreTimeofday (base name: "score_timeofday")', function() {
      // uncomment below and update the code to test the property scoreTimeofday
      //var instane = new PlatformApi.JourneyModel();
      //expect(instance).to.be();
    });

  });

}));
